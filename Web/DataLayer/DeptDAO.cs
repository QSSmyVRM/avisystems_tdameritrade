/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Collections;
using System.Xml;

using NHibernate;
using NHibernate.Criterion;
using System.Collections.Generic;

namespace myVRM.DataLayer
{
    /// <summary>
    /// Data Access Object for Departments.
    /// Implement IDispose to close session
    /// </summary>
    public class deptDAO : vrmDAO
    {
        public deptDAO(string config, log4net.ILog log) : base(config, log) { }


        public IDeptDao GetDeptDao() { return new DeptDao(m_configPath); }

        public class DeptDao :
               AbstractPersistenceDao<vrmDept, int>, IDeptDao
        {
            public DeptDao(string ConfigPath) : base(ConfigPath) { }

        }
        public IUserDeptDao GetUserDeptDao() { return new userDeptDao(m_configPath); }

        public class userDeptDao :
               AbstractPersistenceDao<vrmUserDepartment, int>, IUserDeptDao
        {
            public userDeptDao(string ConfigPath) : base(ConfigPath) { }

        }

        public IDeptApproverDao GetDeptApproverDao() { return new DeptApproverDao(m_configPath); }

        public class DeptApproverDao :
               AbstractPersistenceDao<vrmDeptApprover, int>, IDeptApproverDao
        {
            public DeptApproverDao(string ConfigPath) : base(ConfigPath) { }

        }

        public class DeptCustomAttrDao :
               AbstractPersistenceDao<vrmDeptCustomAttr, int>, IDeptCustomAttrDao
        {
            public DeptCustomAttrDao(string ConfigPath) : base(ConfigPath) { }

        }
        public class DeptCustomAttrExDao :
                   AbstractPersistenceDao<vrmDeptCustomAttrEx, int>, IDeptCustomAttrExDao
        {
            public DeptCustomAttrExDao(string ConfigPath) : base(ConfigPath) { }

        }
        public class DeptCustomAttrOptionDao :
                         AbstractPersistenceDao<vrmDeptCustomAttrOption, int>, IDeptCustomAttrOptionDao
        {
            public DeptCustomAttrOptionDao(string ConfigPath) : base(ConfigPath) { }

        }

        public IDeptCustomAttrDao GetDeptCustomAttrDao(){return new DeptCustomAttrDao(m_configPath);} 
        public IDeptCustomAttrExDao GetDeptCustomAttrExDao (){return new DeptCustomAttrExDao(m_configPath);}
        public IDeptCustomAttrOptionDao GetDeptCustomAttrOptionDao() { return new DeptCustomAttrOptionDao(m_configPath); }
        //FB 1970 start
        public class CustomLanguageDao : AbstractPersistenceDao<VrmCustomLanguage, int>, ICustomLangDao
        {
            public CustomLanguageDao(string ConfigPath) : base(ConfigPath) { }
            public List<VrmCustomLanguage> GetCustomAttrTitlesByID(int ID)
            {
                List<VrmCustomLanguage> TitleList = new List<VrmCustomLanguage>();
                List<ICriterion> criterionList = new List<ICriterion>();

                criterionList.Add(Expression.Eq("CustomAttributeID", ID));
                TitleList = GetByCriteria(criterionList);
                return TitleList;
                
            }

        }
        public ICustomLangDao GetCustomLangDao()
        {
            return new CustomLanguageDao(m_configPath);
        }
        //FB 1970 end
    }

}
