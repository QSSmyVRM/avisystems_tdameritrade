﻿//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886 End
#region using
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections.Specialized;
using System.Net;
using System.Data.Sql;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using System.Linq;
using System.Xml.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using cryptography;

#endregion
namespace EM7Poll
{
    class EM7Poll
    {
        #region public properties
        string EM7URL = "";
        string EM7UserName = "";
        string EM7Password = "";
        string EM7Port = "";
        string EM7OrgOutXML = "";
        string EM7DeviceOutXML = "";
        string errMsg = "";
        NameValueCollection appSettings = null;
        private NS_DATABASE.Database db = null;
        private MyvrmIntegratedEM7.Logger log = null;
        List<Int32> EM7SiloId = null;

        public class EM7Organization
        {
            public string OrgID { get; set; }
            public string OrgName { get; set; }
        }
        public class EM7DeviceStatus
        {
            public string DeviceIP { get; set; }
            public string DeviceStatus { get; set; }
            public string DeviceCurrentStatus { get; set; }//FB 2616
        }
        #endregion

        #region EM7Poll
        internal EM7Poll()
        {
            db = new NS_DATABASE.Database();
            log = new MyvrmIntegratedEM7.Logger();
            appSettings = ConfigurationManager.AppSettings;
        }
        #endregion

        #region EM7OrgPollStart
        /// <summary>
        /// EM7OrgPollStart
        /// </summary>
        public void EM7OrgPollStart()
        {
            try
            {
                log.WritetoLogfile("********EM7OrgPoll Service Started ***************************");
                RetrieveEM7Credentials();
                RetrieveEM7API();
                UpdatemyVRMEM7Silo();
                log.WritetoLogfile("********EM7OrgPoll Service Completed ***************************");
            }
            catch (Exception ex)
            {
                log.WritetoLogfile(ex.Message);
            }
        }
        #endregion

        #region EM7EndPointPollStart
        public void EM7EndPointPollStart()
        {
            try
            {
                log.WritetoLogfile("********EM7EndPointPoll Service Started ***************************");
                RetrieveEM7Credentials();
                FetchmyVRMEM7OrgId();
                FecthEM7DeviceStatus();
                UpdatemyVRMEM7EnpointStatus();
                log.WritetoLogfile("********EM7EndPointPoll Service Completed ***************************");
            }
            catch (Exception ex)
            {
                log.WritetoLogfile(ex.Message);
            }

        }
        #endregion

        # region RetrieveEM7Credentials
        /// <summary>
        /// RetrieveEM7Credentials
        /// </summary>
        public void RetrieveEM7Credentials()
        {
            cryptography.Crypto crypto = null; //FB 3054
            string encryptpass = "";
            DataSet ds = new DataSet();
            try
            {
                db.FecthEM7Credential(ref ds);

                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            if (ds.Tables[0].Rows[0]["EM7URI"] != null)
                                EM7URL = ds.Tables[0].Rows[0]["EM7URI"].ToString().Trim();

                            if (ds.Tables[0].Rows[0]["EM7Username"] != null)
                                EM7UserName = ds.Tables[0].Rows[0]["EM7Username"].ToString().Trim();

                            if (ds.Tables[0].Rows[0]["EM7Password"] != null)
                            {
                                crypto = new Crypto(); //FB 3054 Starts
                                encryptpass = crypto.decrypt(ds.Tables[0].Rows[0]["EM7Password"].ToString().Trim());
                                EM7Password = encryptpass; //FB 3054 Starts
                            }
                            if (ds.Tables[0].Rows[0]["EM7Port"] != null)
                                EM7Port = ds.Tables[0].Rows[0]["EM7Port"].ToString().Trim();
                            if (EM7Port == "443")
                                EM7URL = "https://"+EM7URL;
                            else
                                EM7URL = "http://" + EM7URL; //FB 2616
                        }
                        log.WritetoLogfile("EM7URL=" + EM7URL + "  EM7Username" + EM7UserName + " EM7Password" + EM7Password+" EM7Port:" + EM7Port);
                    }
                }
            }
            catch (Exception e)
            {
                log.WritetoLogfile(e.Message);
            }
        }
        #endregion

        # region UpdatemyVRMEM7Silo
        /// <summary>
        /// UpdatemyVRMEM7Silo
        /// </summary>
        public void UpdatemyVRMEM7Silo()
        {
            log.WritetoLogfile("************************Update EM7 Silo in Myvrm***************");
            XDocument xmlDoc = null;
            bool ret = false;
            try
            {
                if (EM7OrgOutXML.IndexOf("<error>") < 0)
                {
                    xmlDoc = XDocument.Parse(EM7OrgOutXML);
                    var EM7Data = (from data in xmlDoc.Descendants("result_set").Elements("link")
                                   select new EM7Organization
                                 {
                                     OrgID = data.Attribute("URI").Value,
                                     OrgName = data.Attribute("description").Value

                                 }).ToList();

                    ret = db.InsertEM7SiloData(EM7Data);
                    if (!ret)
                        log.WritetoLogfile("Update EM7 Silo in Myvrm Failed");
                    else
                        log.WritetoLogfile("Update EM7 Silo in Myvrm Succeed");
                }
            }
            catch (Exception ex)
            {
                log.WritetoLogfile(ex.Message);
            }

        }
        #endregion

        # region RetrieveEM7API
        /// <summary>
        /// RetrieveEM7API   -  Gowniyan will handle this part
        /// </summary>
        public void RetrieveEM7API()
        {
            string EM7URI = "";
            try
            {
                log.WritetoLogfile("******** Retrieve EM7 API **********************************");
                EM7URI = "organization?limit=100";
                EM7OrgOutXML = WebServiceCall(EM7URI);
            }
            catch (Exception ex)
            {
                errMsg = ex.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                log.WritetoLogfile(ref errMsg);
            }

        }
        #endregion

        # region FetchmyVRMEM7OrgId
        /// <summary>
        /// FetchmyVRMEM7OrgId
        /// </summary>
        private void FetchmyVRMEM7OrgId()
        {
            EM7SiloId = new List<int>();
            try
            {
                db.FetchmyVRMEM7Silo(ref EM7SiloId);
            }
            catch (Exception ex)
            {
                log.WritetoLogfile(ex.Message);
            }
        }
        #endregion

        #region  FecthEM7DeviceStatus
        /// <summary>
        /// FecthEM7DeviceStatus - Gowniyan will Handle
        /// </summary>
        private void FecthEM7DeviceStatus()
        {
            string SiloID = "";
            string EM7URI = "";
            try
            {
                log.WritetoLogfile("******** Retrieve EM7 DeviceStatus **********************************");
                for (int i = 0; i < EM7SiloId.Count; i++)
                {

                    if(SiloID == "")
                        SiloID = EM7SiloId[i].ToString();
                    else
                        SiloID += "," + EM7SiloId[i].ToString(); 
  
                }
                EM7URI = "device?limit=100&extended_fetch=1&filter.organizations.in=[" + SiloID + "]&filter.ip.not=";
                EM7DeviceOutXML = WebServiceCall(EM7URI);

            }
            catch (Exception ex)
            {
                log.WritetoLogfile(ex.Message);
            }
        }
        #endregion

        #region  UpdatemyVRMEM7EnpointStatus
        /// <summary>
        /// FecthEM7DeviceStatus - Gowniyan will Handle
        /// </summary>
        private void UpdatemyVRMEM7EnpointStatus()
        {
            //int EM7OnlineStatus = 0; Commented for FB 2616
            XDocument xmlDoc = new XDocument();
            Hashtable DeviceHashTable = new Hashtable();
            List<string> DeviceIPAddress = null; //ZD 100825
            bool ret = false;
            try
            {
                log.WritetoLogfile("************************Update EM7 EndPoint in Myvrm***************");
                xmlDoc = XDocument.Parse(EM7DeviceOutXML);
                log.WritetoLogfile(xmlDoc.ToString());
                if (EM7DeviceOutXML.IndexOf("<error>") < 0)
                {
                    var EM7DeviceState = (from data in xmlDoc.Descendants("device")
                                          select new EM7DeviceStatus
                                          {
                                              DeviceIP = data.Element("ip").Value,
                                              DeviceStatus = data.Element("state").Value,
                                              DeviceCurrentStatus = data.Element("state").Value //FB 2616
                                          }).ToList();


                    DeviceIPAddress = new List<string>();//ZD 100825
                    for (int i = 0; i < EM7DeviceState.Count; i++)
                    {
                        //if (EM7DeviceState[i].DeviceStatus == "3") Commented for FB 2616
                        //    EM7OnlineStatus = 1;
                        //else
                        //    EM7OnlineStatus = 0;
                        DeviceHashTable.Add(EM7DeviceState[i].DeviceIP, EM7DeviceState[i].DeviceStatus);
                        //ZD 100825 start
                        if (!DeviceIPAddress.Contains(EM7DeviceState[i].DeviceIP))
                            DeviceIPAddress.Add(EM7DeviceState[i].DeviceIP);
                        //ZD 100825 End
                    }
                    ret = db.UpdateEndpointStatus(DeviceHashTable, DeviceIPAddress);//ZD 100825
                    if (!ret)
                        log.WritetoLogfile("************************Update EM7 EndPoint in Myvrm Failed***************");
                    else
                        log.WritetoLogfile("************************Update EM7 EndPoint in Myvrm Succeed***************");

                }
            }
            catch (Exception ex)
            {
                log.WritetoLogfile(ex.Message);
            }
        }
        #endregion

        #region WebServiceCall
        /// <summary>
        /// WebServiceCall
        /// </summary>
        private string WebServiceCall(string EM7URI)
        {
            string outXML = "";
            try
            {
                log.WritetoLogfile("**************Getting into Web Request Call*************");
                string url = EM7URL + "/api/" + EM7URI;
                log.WritetoLogfile("EM7 URL:" + url);
                string authInfo = EM7UserName + ":" + EM7Password;
                log.WritetoLogfile("EM7 Authentication info:" + authInfo);
                String credentials = Convert.ToBase64String(Encoding.UTF8.GetBytes(authInfo));
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                    ((sender1, certificate, chain, sslPolicyErrors) => true);
                ServicePointManager.Expect100Continue = false;


                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Accept = "application/xml, */*.";
                request.MediaType = "HTTP/1.1";
                request.KeepAlive = true;
                request.Headers["Authorization"] = "Basic " + credentials;
                request.Method = "GET";

                WebResponse response = request.GetResponse();
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    outXML = reader.ReadToEnd();
                }
                if (response.Headers["http_code"] != null)
                {

                    outXML += "Error :" + response.Headers["http_code"];
                }
                log.WritetoLogfile("*******************************Received XML:" + outXML);
                return outXML;

            }
            catch (WebException wex)
            {
                String error = "";
                if (wex.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)wex.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            error = " Error from response : " + reader.ReadToEnd();
                        }
                    }
                }
                error = " message from wex" + wex.Message;
                outXML = "Error from web exception: " + error;
            }

            catch (Exception ex)
            {
                outXML = "Error Error Pls help Message:" + ex.Message + " Stactarce:" + ex.StackTrace;
            }
            log.WritetoLogfile("*******************************Received XML:" + outXML);
            return outXML;
        }
        #endregion

    }
}




