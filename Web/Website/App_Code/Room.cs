//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End //ZD 100886
using System;
using System.Data;
using System.Xml;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Text;
using System.Linq;
using System.IO;


/// <summary>
/// Summary description for Tier1
/// </summary>
/// 
namespace ns_DataImport
{
    public class Room: Page
    {
        int externalDatabaseType;
        DataTable masterDT;
        string configPath;
        ns_Logger.Logger log;
        myVRMNet.NETFunctions obj;
        DataTable dtZone;//FB 2519
        DataView dvZone;//FB 2519
        System.Web.UI.WebControls.DropDownList lstTimeZone;
        MyVRMNet.LoginManagement obj1; //ZD 100456

        //protected Enyim.Caching.MemcachedClient memClient = null;//ZD 103496 //ZD 104482
        //protected int memcacheEnabled = 0;//ZD 103496 //ZD 104482

        public Room(int external, DataTable masterDataTable, string config)
        {
            externalDatabaseType = external;
            masterDT = masterDataTable;
            configPath = config;
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions();
            obj1 = new MyVRMNet.LoginManagement();//ZD 100456
        }

        public bool Process(ref int cnt, ref Boolean alluserimport, ref DataTable dterror, ref DataTable dtInformation)//ZD 104091
        {
            //ZD 100456
            XmlDocument failedlist = new XmlDocument();
            XmlNodeList nodelist2;
            XmlNode newnode = null;
            DataRow errow = null;

            string tzID = "-1";
            String inXML = "";
            string eptnme = "";//FB 2362
            
            /* Code coomented for ZD 104482
            XmlDocument loadRooms = null; //ZD 103569
            string roomId = "";
            XmlNode ndeRoomID = null;

            if (HttpContext.Current.Session["MemcacheEnabled"] != null && !string.IsNullOrEmpty(HttpContext.Current.Session["MemcacheEnabled"].ToString()))
                int.TryParse(HttpContext.Current.Session["MemcacheEnabled"].ToString(), out memcacheEnabled);//ZD 103569 End
            */
            inXML += "<GetLocations>";
            inXML += obj.OrgXMLElement();//Organization Module Fixes
            inXML += "  <UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID>";
            inXML += "</GetLocations>";
            String outXML = obj.CallMyVRMServer("GetLocations", inXML, configPath);
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(outXML);
            //XmlNodeList nodesTier1 = xmldoc.SelectNodes("//locationList/tier1List/tier1");
            XmlNodeList nodesTier1 = xmldoc.SelectNodes("//GetLocations/Location");

            //inXML = "<login><userID>11</userID></login>";
            inXML = "";
            inXML += "<login>";
            inXML += "  <userID>" + HttpContext.Current.Session["userID"].ToString() + "</userID>";
            inXML += obj.OrgXMLElement();
            inXML += "</login>";
            outXML = obj.CallMyVRMServer("GetManageDepartment", inXML, configPath);
            XmlDocument xmldoc1 = new XmlDocument();
            xmldoc1.LoadXml(outXML);
            XmlNodeList nodesDept = xmldoc1.SelectNodes("//getManageDepartment/departments/department");
            int i = 0;

            //ZD 104091
            //lstTimeZone = new DropDownList();
            //lstTimeZone.DataValueField = "timezoneID";
            //lstTimeZone.DataTextField = "timezoneName";
            //obj.GetTimezones(lstTimeZone, ref tzID);

            //ZD 104091
            DropDownList lstCountries = new DropDownList();
            DropDownList lstStates = new DropDownList();
            lstStates.ID = "Country";
            lstCountries.DataValueField = "ID";
            lstCountries.DataTextField = "Name";
            obj.GetCountryCodes(lstCountries);
            lstStates.ID = "State";
            lstStates.DataValueField = "ID";
            lstStates.DataTextField = "Code";
            obj.GetCountryStates(lstStates,"");

            int j = 0;
            foreach (DataRow dr in masterDT.Rows)
            {
                errow = dterror.NewRow(); //ZD 100456
                int errorusernum = 0;
                errorusernum = j + 2;
                //ZD 103896
                if (dr["id"].ToString().ToLower().IndexOf(obj.GetTranslatedText("note").ToLower()) >= 0)
                    break;

                if ((dr[obj.GetTranslatedText("Room Name")].ToString().IndexOf("(none)") < 0) 
                    && (dr[obj.GetTranslatedText("Tier One")].ToString().IndexOf("(none)") < 0) && (dr[obj.GetTranslatedText("Tier Two")].ToString().IndexOf("(none)") < 0))
                {
                    i++;

                    //ZD 100456

                    if (dr[obj.GetTranslatedText("Room Name")].ToString().Trim() == "")
                    {
                        errow["Row No"] = errorusernum.ToString();
                        errow["Reason"] = obj.GetTranslatedText("Room doesn't have the name.");
                        alluserimport = false;
                        dterror.Rows.Add(errow);
                        continue;
                    }

                    int tier1ID = 0;
                    if (dr[obj.GetTranslatedText("Tier One")].ToString().Trim().IndexOf("(none)") < 0)
                        tier1ID = GetTier1ID(nodesTier1, dr[obj.GetTranslatedText("Tier One")].ToString().Trim());

                    if (tier1ID <= 0)
                    {
                        errow["Row No"] = errorusernum.ToString();
                        if (dr[obj.GetTranslatedText("Tier One")].ToString().Trim() == "")
                            errow["Reason"] = obj.GetTranslatedText("Room doesn't have the Tier 1");
                        else
                            errow["Reason"] = obj.GetTranslatedText("Given Tier 1 doesn't exists.");
                        alluserimport = false;
                        dterror.Rows.Add(errow);
                        continue;
                    }

                    //ZD 100456
                    String deptXML = "";
                    if (dr[obj.GetTranslatedText("Department")].ToString().Trim().IndexOf("(none)") < 0)
                        deptXML = GetDepartmentID(nodesDept, dr[obj.GetTranslatedText("Department")].ToString().Trim());


                    int tier2ID = 0;
                    if (dr[obj.GetTranslatedText("Tier Two")].ToString().Trim().IndexOf("(none)") < 0)
                        tier2ID = GetTier2ID(tier1ID, dr[obj.GetTranslatedText("Tier Two")].ToString().Trim());

                    if (tier2ID <= 0)
                    {
                        errow["Row No"] = errorusernum.ToString();
                        if (dr[obj.GetTranslatedText("Tier Two")].ToString().Trim() == "")
                            errow["Reason"] = obj.GetTranslatedText("Room doesn't have the Tier 2");
                        else
                            errow["Reason"] = obj.GetTranslatedText("Given Tier 2 doesn't exists.");
                        alluserimport = false;
                        dterror.Rows.Add(errow);
                        continue;
                    }

                    eptnme = dr[obj.GetTranslatedText("Endpoint Name")].ToString();//FB 2362

                    if (eptnme.Trim() == "") //ZD 100456
                        eptnme = dr[obj.GetTranslatedText("Room Name")].ToString();

                    if (eptnme == "")
                        continue;

                    inXML = "<SearchEndpoint>";
                    inXML += "  <UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID>";
                    inXML += obj.OrgXMLElement();//FB 2362
                    inXML += "  <EndpointName>" + eptnme.Trim() + "</EndpointName>";//FB 2362
                    inXML += "  <EndpointType></EndpointType>";
                    inXML += "  <PageNo>1</PageNo>";
                    inXML += "</SearchEndpoint>";
                    outXML = obj.CallMyVRMServer("SearchEndpoint", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                    XmlDocument xmldocEP = new XmlDocument();
                    xmldocEP.LoadXml(outXML);
                    //int endpointID = GetEndpointID(nodesEP, dr["Room"].ToString());
                    String endpointID = "0";
                    if (xmldocEP.SelectNodes("//SearchEndpoint/Endpoints/Endpoint").Count > 0)
                        endpointID = xmldocEP.SelectSingleNode("//SearchEndpoint/Endpoints/Endpoint/ID").InnerText;
                    
                    //ZD 100456
                    String roomID = "";
                    if (dr["id"].ToString() != "")
                    {
                        roomID = dr["id"].ToString();
                        obj1.simpleDecrypt(ref roomID); 
                    }
                    //ZD 104091
                    /*
                    StringBuilder rmAdXMl = new StringBuilder();
                    if (dr[obj.GetTranslatedText("Room Administrator")].ToString() != "" && dr[obj.GetTranslatedText("Room Administrator")].ToString().Trim().IndexOf(' ') > 0)
                    {
                        rmAdXMl.Append("<login>");
                        rmAdXMl.Append(obj.OrgXMLElement());//Organization Module Fixes
                        rmAdXMl.Append("<userID>" + HttpContext.Current.Session["userID"].ToString() + "</userID>");
                        rmAdXMl.Append("<user>");
                        rmAdXMl.Append("<firstName>" + dr[obj.GetTranslatedText("Room Administrator")].ToString().Split(' ')[0] + "</firstName>");
                        rmAdXMl.Append("<lastName>" + dr[obj.GetTranslatedText("Room Administrator")].ToString().Split(' ')[1] + "</lastName>");
                        rmAdXMl.Append("<email></email><type></type>");
                        rmAdXMl.Append("</user>");
                        rmAdXMl.Append("</login>");
                    }
                    String outRmXML = obj.CallMyVRMServer("SearchUserOrGuest", rmAdXMl.ToString(), configPath);
                    XmlDocument xmldocc = new XmlDocument();
                    xmldocc.LoadXml(outRmXML);
                    XmlNodeList nodes = xmldocc.SelectNodes("//users/user");
                    if (nodes.Count > 0)
                    {
                        rmAdminID = nodes[0].SelectSingleNode("userID").InnerText;
                    }
                    */
                    String rmAdminID = "";
                    if (dr[obj.GetTranslatedText("Room Administrator")].ToString().Trim() != "")
                        rmAdminID = dr[obj.GetTranslatedText("Room Administrator")].ToString();

                    if (dr[obj.GetTranslatedText("Room Name")].ToString().Trim() != "")//FB 2362
                    {

                        String videoType = "";

                        videoType = dr[obj.GetTranslatedText("Media (None Audio-only Audio-Video)")].ToString().ToLower().Trim();

                        if (videoType == obj.GetTranslatedText("audio-only"))
                            videoType = "1";
                        else if (videoType.IndexOf(obj.GetTranslatedText("video")) > 0)// == "Audio, Video")
                            videoType = "2";
                        else
                            videoType = "0";

                        String tzone = "";
                        if (masterDT.Columns.Contains(obj.GetTranslatedText("Time zone")))
                            tzone = dr[obj.GetTranslatedText("Time zone")].ToString().Trim();
                        else
                            tzone = dr[obj.GetTranslatedText("Timezone")].ToString().Trim();

                        //ZD 104091 - Start
                        string icontrol = "0";
                        if(dr[obj.GetTranslatedText("iControl Room")].ToString().ToLower() == "yes")
                            icontrol = "1";

                        string capacity="0", add1 = "", add2 = "", city = "", postalCode = "", country = "", state = "", roomQueue = "";
                        add1 = dr[obj.GetTranslatedText("Street Address 1")].ToString();
                        add2 = dr[obj.GetTranslatedText("Street Address 2")].ToString();
                        city = dr[obj.GetTranslatedText("City")].ToString();
                        roomQueue = dr[obj.GetTranslatedText("Room Queue")].ToString();

                        country = "225";
                        try
                        {
                            if (dr[obj.GetTranslatedText("Country")].ToString() != "")
                                country = lstCountries.Items.FindByText(dr[obj.GetTranslatedText("Country")].ToString()).Value;
                        }
                        catch (Exception ex) { }

                        state = "34";
                        try
                        {
                            if (dr[obj.GetTranslatedText("State/Province")].ToString() != "")
                                state = lstStates.Items.FindByText(dr[obj.GetTranslatedText("State/Province")].ToString()).Value;
                        }
                        catch (Exception ex) { }

                        postalCode = dr[obj.GetTranslatedText("Postal Code")].ToString();
                        capacity = dr[obj.GetTranslatedText("Maximum Capacity")].ToString().Trim();
                        
                        if (capacity != "")
                        {
                            Int32 valCapacity = Int32.Parse(capacity);

                            if (valCapacity < 0 || valCapacity > 10000)
                            {
                                errow["Row No"] = errorusernum.ToString();
                                errow["Reason"] = obj.GetTranslatedText("Maximum Capacity between 0 and 10000");
                                alluserimport = false;
                                dterror.Rows.Add(errow);
                                continue;
                            }
                        }
                        if (capacity.Trim() == "")
                            capacity = "0";

                        String confAppr1 = "", confAppr2 = "", confAppr3 = "";
                        if (dr[obj.GetTranslatedText("Conference Approver 1")].ToString().Trim() != "")
                            confAppr1 = dr[obj.GetTranslatedText("Conference Approver 1")].ToString();

                        if (dr[obj.GetTranslatedText("Conference Approver 2")].ToString().Trim() != "")
                            confAppr2 = dr[obj.GetTranslatedText("Conference Approver 2")].ToString();

                        if (dr[obj.GetTranslatedText("Conference Approver 3")].ToString().Trim() != "")
                            confAppr3 = dr[obj.GetTranslatedText("Conference Approver 3")].ToString();
                        
                        string imgPath = "../image/room/"; 
                        string rmImageNames = "", map1Img = "", map2Img = "", misc1Img = "", misc2Img = "";
                        rmImageNames = dr[obj.GetTranslatedText("Room Image")].ToString();
                        string strImgMissing = "#";
                        string strImgWidthExceeds = "#";
                        string errMsg = "";
                        if (rmImageNames.Trim() != "")
                        {
                            if (!ImageFileMovetoOtherFolder(rmImageNames, imgPath, "Room Image", ref errMsg))
                            {
                                rmImageNames = "";
                                if(errMsg == "W")
                                    strImgWidthExceeds += ", " + obj.GetTranslatedText("Room image width and height should not exceed 250 pixels.");
                                else
                                    strImgMissing += ", " + obj.GetTranslatedText("Room Image");
                            }
                        }
                        map1Img = dr[obj.GetTranslatedText("Map 1")].ToString();
                        if (map1Img.Trim() != "")
                        {
                            if (!ImageFileMovetoOtherFolder(map1Img, imgPath, "Map 1", ref errMsg))
                            {
                                map1Img = "";
                                if (errMsg == "W")
                                    strImgWidthExceeds += ", " + obj.GetTranslatedText("Map 1 attachment width and height should not exceed 250 pixels.");
                                else
                                    strImgMissing += ", " + obj.GetTranslatedText("Map 1");
                            }
                        }
                        map2Img = dr[obj.GetTranslatedText("Map 2")].ToString();
                        if (map2Img.Trim() != "")
                        {
                            if (!ImageFileMovetoOtherFolder(map2Img, imgPath, "Map 2", ref errMsg))
                            {
                                map2Img = "";
                                if (errMsg == "W")
                                    strImgWidthExceeds += ", " + obj.GetTranslatedText("Map 2 attachment width and height should not exceed 250 pixels.");
                                else                                    
                                    strImgMissing += ", " + obj.GetTranslatedText("Map 2");
                            }
                        }
                        misc1Img = dr[obj.GetTranslatedText("Misc. 1")].ToString();
                        if (misc1Img.Trim() != "")
                        {
                            if (!ImageFileMovetoOtherFolder(misc1Img, imgPath, "Misc 1", ref errMsg))
                            {
                                misc1Img = "";
                                if (errMsg == "W")
                                    strImgWidthExceeds += ", " + obj.GetTranslatedText("Misc 1 attachment width and height should not exceed 250 pixels.");
                                else                                    
                                    strImgMissing += ", " + obj.GetTranslatedText("Misc 1");
                            }
                        }
                        misc2Img = dr[obj.GetTranslatedText("Misc. 2")].ToString();
                        if (misc2Img.Trim() != "")
                        {
                            if (!ImageFileMovetoOtherFolder(misc2Img, imgPath, "Misc 2", ref errMsg))
                            {
                                misc2Img = "";
                                if (errMsg == "W")
                                    strImgWidthExceeds += ", " + obj.GetTranslatedText("Misc 2 attachment width and height should not exceed 250 pixels.");
                                else 
                                    strImgMissing += ", " + obj.GetTranslatedText("Misc 2");
                            }
                        }

                        string reason = "";
                        if (strImgWidthExceeds != "#")
                        {

                            strImgWidthExceeds = strImgWidthExceeds.Replace("#,", "");
                            reason = strImgWidthExceeds;

                        }

                        if (strImgMissing != "#")
                        {
                            strImgMissing = strImgMissing.Replace("#,", "");
                            if (reason != "")
                                reason += " and " + obj.GetTranslatedText("Image missing for") + ' ' + strImgMissing;
                            else
                                reason = obj.GetTranslatedText("Image missing for") + ' ' + strImgMissing;
                        }

                        if (reason != "")
                        {
                            DataRow inforow = dtInformation.NewRow();
                            inforow["Row No"] = errorusernum.ToString();
                            inforow["Reason"] = reason;
                            alluserimport = false;
                            dtInformation.Rows.Add(inforow);
                        }
                        //log.Trace("<br>" + i + ": tier1ID: " + tier1ID + " : " + dr[obj.GetTranslatedText("Tier One")] + "tier2ID: " + tier2ID + " : " + deptXML + " : " + dr[obj.GetTranslatedText("Department")].ToString());
                        //ZD 104091 - End
                        String roomInXML = Create_roomInXMLNew(deptXML, tier1ID, tier2ID, endpointID, dr[obj.GetTranslatedText("Room Name")].ToString()
                            , GetTimeZoneID(tzone), roomID, dr[obj.GetTranslatedText("Floor")].ToString(), dr[obj.GetTranslatedText("Room #")].ToString()
                            , dr[obj.GetTranslatedText("Room Phone")].ToString(), dr[obj.GetTranslatedText("Projector Available")].ToString(), rmAdminID, videoType
                            , capacity, icontrol, add1, add2, city, postalCode, country, state, confAppr1, confAppr2, confAppr3, roomQueue
                            , rmImageNames, map1Img, map2Img, misc1Img, misc2Img, imgPath); //FB 2519 //ZD 100456 //ZD 104091
                        
                        outXML = obj.CallMyVRMServer("SetRoomProfile", roomInXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

                        if (outXML.IndexOf("<error>") >= 0)
                        {
                            //write to a log file                 
                            log.Trace(obj.ShowErrorMessage(outXML));

                            //ZD 100456
                            alluserimport = false;
                            failedlist.LoadXml(outXML);
                            newnode = failedlist.SelectSingleNode("//error/message");
                            nodelist2 = failedlist.SelectNodes("//error/message");
                            if (nodelist2.Count > 0)
                            {
                                errow["Row No"] = errorusernum.ToString();
                                errow["Reason"] = newnode.InnerXml;
                                dterror.Rows.Add(errow);
                            }
                        }
                        else
                        {
                            /* commented for ZD 104482
                            //ZD 103496 Starts
                            DataTable dtTable = null;
                            XmlNode node = null;
                            string disabled = "0";

                            loadRooms = new XmlDocument();
                            loadRooms.LoadXml(outXML);

                            ndeRoomID = loadRooms.SelectSingleNode("Rooms/Room/RoomID");
                            if (ndeRoomID != null)
                                roomId = ndeRoomID.InnerText;
                            node = loadRooms.SelectSingleNode("Rooms/Room/Disabled");
                            if (node != null)
                                disabled = node.InnerText.Trim();

                            #region Memcache
                            DataTable dtaddTable = null;
                            DataRow[] RoomOldRows = null;
                            DataSet dsCache = null;
                            DataView dv = null;
                            DataTable dtNewTable = null;
                            bool bRet = false;
                            if (memcacheEnabled == 1)
                            {
                                using (memClient = new Enyim.Caching.MemcachedClient())
                                {
                                    dtNewTable = new DataTable();
                                    obj.GetTablefromCache(ref memClient, ref dsCache);

                                    if (dsCache.Tables.Count > 0)
                                        dtaddTable = dsCache.Tables[0];

                                    bRet = obj.GetAllRoomsInfo(roomId, disabled, ref dtNewTable);

                                    if (!bRet || dtNewTable.Rows.Count <= 0)
                                    {
                                        //dtTable = memClient.Get<DataTable>("myVRMRoomsDatatable");
                                        obj.GetAllRoomsInfo("", "0", ref dtaddTable, ref dsCache);
                                    }
                                    RoomOldRows = dtaddTable.Select("Roomid = '" + roomId + "'");

                                    if (RoomOldRows != null && RoomOldRows.Count() > 0)
                                    {
                                        dtaddTable.Rows.Remove(RoomOldRows[0]);
                                        dtaddTable.ImportRow(dtNewTable.Rows[0]);
                                    }
                                    else
                                        dtaddTable.ImportRow(dtNewTable.Rows[0]);


                                    dv = new DataView(dtaddTable);
                                    dv.Sort = "Tier1Name,Tier2Name,RoomName ASC";
                                    dtaddTable = dv.ToTable();

                                    obj.AddRoomTabletoCache(ref memClient, ref dsCache);
                                    //memClient.Store(Enyim.Caching.Memcached.StoreMode.Set, "myVRMRoomsDatatable", dtTable);

                                }
                            }

                            #endregion

                            //ZD 103496 End
                            */ 
                            log.Trace("Success");
                            cnt++;
                        }
                    }
                }

                j = j + 1;//ZD 100456
            }

            // put that in a messenger object 
            // call the com cmd 
            // get the tiers 
            // save the info in csv file
            return true;
        }

        protected bool ImageFileMovetoOtherFolder(String fileNames, String imgPath, String folderName , ref String errMsg)
        {
            try
            {
                string from = "",to ="";
                string[] roomNameList = fileNames.Split(',');
                errMsg = "";
                for (int k = 0; k < roomNameList.Length; k++)
                {
                    from = Server.MapPath("..").Replace("Website", "") + "DataUpload" + "\\" + folderName + "\\" + roomNameList[0];
                    to = Server.MapPath("..") + "\\image\\room\\" + roomNameList[0];

                    if (File.Exists(to))
                        return true;
                    else if (File.Exists(from))
                    {
                        
                        System.Drawing.Image imgFile = System.Drawing.Image.FromFile(from);

                        if (imgFile.Width > 250 || imgFile.Height > 250)
                        {
                            errMsg = "W"; // Width Exceeds
                            return false;
                        }
                        imgFile.Dispose();

                        File.Move(from, to);
                    }
                    else if (!File.Exists(from) && File.Exists(to))
                    {
                        return true;
                    }
                    else
                        return false;

                    /*
                    try
                    {
                        File.Move(from, to); // Try to move                        
                    }
                    catch (System.IO.FileNotFoundException e)
                    {
                        return true;
                        //return false;
                        log.Trace(e.Message);
                    }
                     * */
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        protected int GetTier1ID(XmlNodeList nodes, String tier1Name)
        {
            try
            {
                foreach (XmlNode node in nodes)
                {
                    if (node.SelectSingleNode("Name").InnerText.ToLower().Trim().Equals(tier1Name.Trim().ToLower()))
                        return Int32.Parse(node.SelectSingleNode("ID").InnerText);
                }
                return -1;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return -1;
            }
        }
        protected int GetTier2ID(int tier1ID, String tier2Name)
        {
            try
            { 

                //String inXML = "<login><userID>11</userID><tier1ID>" + tier1ID + "</tier1ID></login>";
                String inXML = "";
                inXML += "<GetLocations2>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID>";
                inXML += "  <Tier1ID>" + tier1ID + "</Tier1ID>";
                inXML += "</GetLocations2>";
                String outXML = obj.CallMyVRMServer("GetLocations2", inXML, configPath);
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//GetLocations2/Location");
                foreach (XmlNode node in nodes)
                {
                    if (node.SelectSingleNode("Name").InnerText.ToLower().Trim().Equals(tier2Name.ToLower().Trim()))
                        return Int32.Parse(node.SelectSingleNode("ID").InnerText);
                }
                return -1;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return -1;
            }
        }

        //ZD 100456
        protected String GetDepartmentID(XmlNodeList nodes, String deptName)
        {
            try
            {
                String deptXML = "";
                String[] strDept = null;
                strDept = deptName.Split(',');

                foreach (String dept in strDept)
                {
                    foreach (XmlNode node in nodes)
                    {
                        if (node.SelectSingleNode("name").InnerText.Trim().ToLower().Equals(dept.Trim().ToLower()))
                            deptXML += "<Department><ID>" + node.SelectSingleNode("id").InnerText + "</ID><Name>"
                                + node.SelectSingleNode("name").InnerText + "</Name><SecurityKey></SecurityKey></Department>";
                            //return Int32.Parse(node.SelectSingleNode("id").InnerText);
                    }
                }
                return deptXML;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return "";
            }
        }

        protected int GetEndpointID(XmlNodeList nodes, String EPName)
        {
            try
            {
                foreach (XmlNode node in nodes)
                {
                    if (node.SelectSingleNode("EndpointName").InnerText.Trim().Equals(EPName.Trim()))
                        return Int32.Parse(node.SelectSingleNode("ID").InnerText);
                }
                return -1;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return -1;
            }
        }

        //FB 2519 Starts
        protected String GetTimeZoneID(String tZoneStr)
        {

            DataSet dsZone = new DataSet();
            myVRMNet.NETFunctions obj;
            String tZoneID = "26";
            String tZone = "";
            String[] tZoneArr = null;
            try
            {
                String zoneInXML = "<GetTimezones><UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID></GetTimezones>";
                String zoneOutXML;
                tZone = tZoneStr;
                if (dtZone == null)
                {
                    if (dsZone.Tables.Count == 0)
                    {
                        obj = new myVRMNet.NETFunctions();
                        zoneOutXML = obj.CallMyVRMServer("GetTimezones", zoneInXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                        obj = null;
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(zoneOutXML);
                        XmlNodeList nodes = xmldoc.SelectNodes("//Timezones/timezones/timezone");
                        if (nodes.Count > 0)
                        {
                            XmlTextReader xtr;
                            foreach (XmlNode node in nodes)
                            {
                                xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                                dsZone.ReadXml(xtr, XmlReadMode.InferSchema);
                            }
                            if (dsZone.Tables.Count > 0)
                            {
                                dvZone = new DataView(dsZone.Tables[0]);
                                dtZone = dvZone.Table;
                            }
                        }
                    }
                }
                if (tZone != "")
                {
                    tZoneArr = tZone.Split(' ');
                    int tzonelength = tZoneArr.Length;
                    String t1 = null, t2 = null;
                    if (tzonelength == 1)
                    {
                        t1 = tZoneArr[0];
                    }
                    else
                    {
                        t1 = tZoneArr[0];
                        t2 = tZoneArr[1];
                    }
                    foreach (DataRow row in dtZone.Rows)
                    {
                        if (tzonelength == 1)
                        {
                            if (row["timezoneName"].ToString().Contains(t1))
                            {
                                tZoneID = row["timezoneID"].ToString();
                                break;
                            }
                        }
                        else
                        {
                            //ZD 100456
                            //ZD 100456
                            if (row["timezoneName"].ToString() == tZone)
                            {
                                tZoneID = row["timezoneID"].ToString();
                                break;
                            }
                            else if (row["StandardName"].ToString().Contains(t1) && row["StandardName"].ToString().Contains(t2))
                            {
                                tZoneID = row["timezoneID"].ToString();
                                break;
                            }
                            else if (row["timezoneName"].ToString().Contains(t1) && row["timezoneName"].ToString().Contains(t2))
                            {
                                tZoneID = row["timezoneID"].ToString();
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return tZoneID;
        }
        //FB 2519 Ends

        private String Create_roomInXMLNew(String deptXML, int tier1ID, int tier2ID, String endpointID, String RoomName, string timezoneID
            , String roomID, String floor, String rmNum, String rmPhone, String prjtAvai, String rmAdminID, String videoType
            , String capacity, String icontrol, String add1, String add2, String city, String postalCode, String country, String state, String confAppr1, String confAppr2, String confAppr3
            , String roomQueue, String rmImageNames, String map1Img, String map2Img, String misc1Img, String misc2Img, String imgPath) //ZD 100456 //ZD 104091
        {
            try
            {
                
                //ZD 100456
                String editfrom = "D";
                if (roomID == "")
                {
                    roomID = "new";
                    editfrom = "ND";
                }
                if (prjtAvai == "" || prjtAvai.Trim().ToLower() == "no")
                    prjtAvai = "0";
                else
                    prjtAvai = "1";

                String inXML = "";
                inXML += "<SetRoomProfile>";
                inXML += obj.OrgXMLElement();
                inXML += "<UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID>"; //ZD 101026
                inXML += "<EditFrom>" + editfrom + "</EditFrom>"; //ZD 100456
                inXML += "<RoomID>" + roomID + "</RoomID>";//ZD 100456
                //inXML += "<RoomName>" + RoomName + " test</RoomName>";
                inXML += "<RoomName>" + RoomName + "</RoomName>";//Code commented for BCS
                inXML += "<RoomPhoneNumber>"+ rmPhone +"</RoomPhoneNumber>"; //ZD 100456
                inXML += "<MaximumCapacity>" + capacity + "</MaximumCapacity>"; //ZD 104091
                inXML += "<MaximumConcurrentPhoneCalls>0</MaximumConcurrentPhoneCalls>";
                //inXML += "<SetupTime>0</SetupTime>";//ZD 101563
                //inXML += "<TeardownTime>0</TeardownTime>";
                //ZD 100456
                if (rmAdminID == "")
                    rmAdminID = "11";

                if (rmAdminID == "11" && HttpContext.Current.Session["organizationID"].ToString() != "11")
                    rmAdminID = "-1";

                inXML += "<AssistantInchargeID>" + rmAdminID + "</AssistantInchargeID>";//FB 2362//FB 2519 //ZD 100456
                inXML += "<AssistantInchargeName></AssistantInchargeName>";
                inXML += "<MultipleAssistantEmails></MultipleAssistantEmails>";
                inXML += "<Tier1ID>" + tier1ID + "</Tier1ID>";
                inXML += "<Tier2ID>" + tier2ID + "</Tier2ID>";
                inXML += "<Floor>" + floor + "</Floor>"; //ZD 100456
                inXML += "<RoomNumber>" + rmNum + "</RoomNumber>"; //ZD 100456
                inXML += "<Handicappedaccess>0</Handicappedaccess>";
                inXML += "<isVIP></isVIP>";
                inXML += "<isTelepresence>0</isTelepresence>";
                inXML += "<ServiceType></ServiceType>";
                inXML += "<DedicatedVideo>0</DedicatedVideo>";//FB 2519

                inXML += "<StreetAddress1>" + add1 + "</StreetAddress1>";
                inXML += "<StreetAddress2>" + add2 + "</StreetAddress2>";
                inXML += "<City>" + city + "</City>";
                inXML += "<ZipCode>" + postalCode + "</ZipCode>";
                inXML += "<Country>" + country + "</Country>";
                inXML += "<State>" + state + "</State>";//FB 2519
                inXML += "<MapLink></MapLink>";
                inXML += "<ParkingDirections></ParkingDirections>";
                inXML += "<AdditionalComments></AdditionalComments>";
                inXML += "<TimezoneID>" + timezoneID + "</TimezoneID>";
                inXML += "<Longitude></Longitude>";
                inXML += "<Latitude></Latitude>";
                inXML += "<RoomImages></RoomImages>";
                inXML += "<RoomImageName>" + rmImageNames + "</RoomImageName>";
                if(rmImageNames != "")
                    inXML += "<RoomImagePath>" + imgPath + "</RoomImagePath>";
                else
                    inXML += "<RoomImagePath></RoomImagePath>";
                //inXML += "<Image>0</Image>";
                //inXML += "</RoomImages>";
                inXML += "<Images>";
                inXML += "<Map1>" + map1Img + "</Map1>";
                if (map1Img != "")
                    inXML += "<Map1Image>" + imgPath + "</Map1Image>";
                else
                    inXML += "<Map1Image></Map1Image>";

                inXML += "<Map2>" + map2Img + "</Map2>";
                if (map2Img != "")
                    inXML += "<Map2Image>" + imgPath + "</Map2Image>";
                else
                    inXML += "<Map2Image></Map2Image>";

                inXML += "<Security1></Security1>";
                inXML += "<Security1ImageId></Security1ImageId>";
                inXML += "<Security2></Security2>";
                inXML += "<Misc1>" + misc1Img + "</Misc1>";
                if (misc1Img != "")
                    inXML += "<Misc1Image>" + imgPath + "</Misc1Image>";
                else
                    inXML += "<Misc1Image></Misc1Image>";

                inXML += "<Misc2>" + misc2Img + "</Misc2>";
                if (misc2Img != "")
                    inXML += "<Misc2Image>" + imgPath + "</Misc2Image>";
                else
                    inXML += "<Misc2Image></Misc2Image>";

                inXML += "</Images>";
                inXML += "<Approvers>";
                inXML += "<Approver1ID>" + confAppr1 + "</Approver1ID>";
                inXML += "<Approver1Name></Approver1Name>";
                inXML += "<Approver2ID>" + confAppr2 + "</Approver2ID>";
                inXML += "<Approver2Name></Approver2Name>";
                inXML += "<Approver3ID>" + confAppr3 + "</Approver3ID>";
                inXML += "<Approver3Name></Approver3Name>";
                inXML += "<ApprovalReq></ApprovalReq>";//FB 2519
                inXML += "</Approvers>";
                inXML += "<EndpointID>" + endpointID + "</EndpointID>";
                inXML += "<Custom1></Custom1>";
                inXML += "<Custom2></Custom2>";
                inXML += "<Custom3></Custom3>";
                inXML += "<Custom4></Custom4>";
                inXML += "<Custom5></Custom5>";
                inXML += "<Custom6></Custom6>";
                inXML += "<Custom7></Custom7>";
                inXML += "<Custom8></Custom8>";
                inXML += "<Custom9></Custom9>";
                inXML += "<Custom10></Custom10>";
                inXML += "<Projector>" + prjtAvai + "</Projector>";//Changed for BCS //ZD 100456

                //ZD 100456
                if (endpointID != "0" && videoType == "0")
                    videoType = "2";
                else if (endpointID == "0")
                    videoType = "0";

                inXML += "<Video>" + videoType + "</Video>";

                //if(endpointID != "0")//FB 2362
                //    inXML += "<Video>2</Video>";
                //else
                //    inXML += "<Video>0</Video>";//FB 2362
                inXML += "<DynamicRoomLayout></DynamicRoomLayout>";
                inXML += "<CatererFacility></CatererFacility>";
                inXML += "<Departments>" + deptXML + "</Departments>"; //ZD 100456
                inXML += "<RoomImage>room1</RoomImage>";
                inXML += "<RoomQueue>" + roomQueue + "</RoomQueue>";
                //FB 2519 Start
                inXML += "<DedicatedCodec>0</DedicatedCodec>";
                inXML += "<AVOnsiteSupportEmail></AVOnsiteSupportEmail>";
                inXML += "<IsVMR>0</IsVMR>";
                inXML += "<InternalNumber></InternalNumber>";
                inXML += "<ExternalNumber></ExternalNumber>";  
                inXML += "<isPublic>0</isPublic>";
                inXML += "<iControlRoom>" + icontrol + "</iControlRoom>";
                //ZD 104091 - Start 
                if(icontrol == "1")
                    inXML += "<RoomCategory>6</RoomCategory>";
                else
                    inXML += "<RoomCategory>1</RoomCategory>";
                inXML += "<Security2Image></Security2Image>";
                inXML += "<RoomURL></RoomURL>";
                //ZD 104591 - Start
                String rmIcon = "";
                //if (icontrol == "0") 104733 //icontrol also need icons
                //{
                    if (videoType == "0")
	                    rmIcon = ns_MyVRMNet.vrmAttributeType.RoomOnly;
	                else if (videoType == "1")
	                    rmIcon = ns_MyVRMNet.vrmAttributeType.AudioOnly;
	                else if (videoType == "2")
	                    rmIcon = ns_MyVRMNet.vrmAttributeType.Video;
                //}
                //ZD 104091 - End
                inXML += "<RoomIconTypeId>"+ rmIcon +"</RoomIconTypeId>";
                //FB 2519 End
                inXML += "</SetRoomProfile>";

                return inXML;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
    }
}