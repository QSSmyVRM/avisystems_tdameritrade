/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 //ZD 100886
using System;
using System.Collections.Generic;
using System.Text;

namespace ns_MyVRMNet
{
    public class vrmConfStatus
    {
        public const String Scheduled = "0";
        public const String Pending = "1";
        public const String Terminated = "3";
        public const String Ongoing = "5";
        public const String OnMCU = "6";
        public const String Completed = "7";
        public const String Deleted = "9";
        public const string WaitList = "2";//ZD 102532
    }
    public class vrmConfType
    {
        public const String Video = "1";
        public const String AudioVideo = "2";
        public const String Immediate = "3";
        public const String P2P = "4"; // Point to point
        public const String Template = "5";
        public const String AudioOnly = "6";
        public const String RoomOnly = "7";
        public const String HotDesking = "8"; //FB 2694
        public const String OBTP = "9"; //ZD 100513
        public const String Phantom = "11";
    }
    public class vrmConfUserType
    {
        public const String External = "1";
        public const String Room = "2";
        public const String CC = "3";
    }
    public class vrmConfUserStatus
    {
        public const String Undecided = "0";
        public const String Accepted = "1";
        public const String Rejectetd = "2";
        public const String Reschedule = "3";
    }
    public class vrmConfEndpointType
    {
        public const String User = "U";
        public const String Room = "R";
        public const String Cascade = "4";
        public const String Guest = "U";
    }
    public class vrmWorkorderType
    {
        public const String AV = "1";
        public const String CAT = "2";
        public const String HK = "3";
    }
    public class vrmConnectionTypes
    {
        public const String DialOutOld = "0";
        public const String DialIn = "1";
        public const String DialOut = "2";
        public const String Direct = "3";
    }
    public class vrmAddressType
    {
        public const String IP = "1";
        public const String H323 = "2";
        public const String E164 = "3";
        public const String ISDN = "4";
        //public const String MPI = "5";//Commented for ZD 103595- Operations - Remove MPI Functionality and Verbiage
        public const String SIP = "6";//FB 3012
    }
    public class vrmVideoProtocol
    {
        public const String IP = "1";
        public const String ISDN = "2";
        public const String SPI = "3";
        //public const String MPI = "4";//Commented for ZD 103595- Operations - Remove MPI Functionality and Verbiage
    }
    public class vrmMCUInterfaceType
    {
        public const String Polycom = "1";
        public const String Codian = "3";
        public const String Tandberg = "4";
    }
    public class vrmClient
    {
        public const String Wustl = "WASHU";
        public const String NGC = "NGC";
        public const String PennState = "PSU";
        public const String LHRIC = "LHRIC";
        public const String MOJ = "MOJ";//Added Client MOJ
        public const String Mayo = "MAYO";//Added Client mayo 1911
    }
    //Code Added For FB 1371 and FB 1367
    public class vrmEndPointConnectionSatus
    {
        public const String disconnect = "0";
        public const String Connecting = "1";//Edited for Blue Status Project
        public const String Connected = "2"; //Edited for Blue Status Project
        public const String Online = "3";//Blue Status Project
        public const String PartiallyConnected = "4";//FB 1824
        //As per Blue Status Project Constants ; 0=disconnect,1=connecting,2=connected,3=online,-1=Unreachable
    }
    public class vrmImageFormattype  // Image Project
    {
        public const String jpg = "1";
        public const String jpeg = "2";
        public const String gif = "3";
        public const String bmp = "4";
        public const String png = "5";
    }
    public class vrmAttributeType  // Image Project
    {
        public const String room = "1";
        public const String roommap1 = "2";
        public const String roommap2 = "3";
        public const String roomsec1 = "4";
        public const String roomsec2 = "5";
        public const String roommisc1 = "6";
        public const String roommisc2 = "7";
        public const String av = "8";
        public const String catering = "9";
        public const String hk = "10";
        public const String banner = "11";  
        public const String highresbanner = "12"; //Organization/CSS Module
        public const String companylogo = "13";
        public const String emaillogo = "14";
        public const String sitelogo = "15";
        public const String MirrorCssXml = "16";
        public const String ArtifactsXml = "17";
        public const String TextChangeXml = "18";
        public const String DefaultCssXML = "19";
        public const String sitebanner = "20";
        public const String sitehighresbanner = "21";
        public const String footerimage = "22"; // FB 1710
        public const String loginbackground = "23"; // FB 2719
        //FB 2065 - Start
        public const string RoomOnly = "Room.jpg";
        public const string AudioOnly = "Audio.jpg";
        public const string Video = "Video.jpg";
        public const string Telepresence = "Telepresence.jpg";
        public const string HotdeskingAudio = "HotdeskingAudio.jpg";
        public const string HotdeskingVideo = "HotdeskingVideo.jpg";
        public const string GuestVideo = "GuestVideo.jpg"; //ZD 100619
        //FB 2065 - End
      }

    //FB 1830
    public class vrmCurrencyFormat
    {
        public const String dollar = "$";
        public const String bound = "€";
    }

    public class vrmNumberFormat
    {
        public const String american = "en-US";
        public const String european = "fr-FR";
    }

    public class vrmSpecialChar
    {
        public const String LessThan = "õ"; //Alt 0245
        public const String GreaterThan = "æ"; //Alt 145
        public const String ampersand = "σ"; //Alt 229
        public const String doubleQuotes = @""""; //ZD 104391
    }

    //FB 2693 Starts
    public class vrmPC 
    {
        public const int BlueJeans = 1;
        public const int Jabber = 2;
        public const int Lync = 3;
        public const int Vidtel = 4;
        public const int Vidyo = 5;
    }
    //FB 2693 Ends
    //FB 3054 Starts
    public class vrmPassword
    {
        public const string MailServer = "*****";
        public const string LDAP = "*****";
        public const string EM7 = "*****";
        public const string External = "*****";
        public const string Whygo = "*****";
        public const string UserProfile = "*****";
        public const string MCUProfile = "*****";
        public const string Vidyo = "*****";
        public const string DMAPW = "*****";
        public const string WebEXPW = "Aa0123"; //ZD 100835
    }
    //FB 3054 Ends
    //FB 3052 Starts
    public class MCUType
    {
        public const int PolycomMGC25 = 1;
        public const int PolycomMGC50 = 2;
        public const int PolycomMGC100 = 3;
        public const int CodianMCU4200 = 4;
        public const int CodianMCU4500 = 5;
        public const int CodianMSE8000Series = 6;
        public const int TandbergMPS800Series = 7;
        public const int PolycomRMX2000 = 8;
        public const int RadvisionScopia = 9;
        public const int LifeSize = 11;
        public const int CiscoMSE8710 = 12;
        public const int PolycomRPRM = 13;
        public const int iView = 14;
        public const int PolycomRMX1000 = 17;
        public const int PolycomRMX1500 = 18;
        public const int PolycomRMX4000 = 19;
    }
    //FB 3052 Ends
    //FB 2639 - Search Start
    public class vrmSearchFilterType
    {
        public const string Ongoing = "1";
        public const string Reservation = "2";
        public const string Public = "3";
        public const string Pending = "4";
        public const string ApprovalPending = "5";
        public const string Congiere = "6";
        public const string Hotdesking = "7";
        public const string custom = "8";
        public const string OnMCU = "9"; //ZD 100036
        public const string DeletedorTerminated = "9"; //ZD 102532
        public const string Completed = "10"; //ZD 102532
        public const string WaitList = "11"; //ZD 102532
    }
    //FB 2639 - Search End
}
