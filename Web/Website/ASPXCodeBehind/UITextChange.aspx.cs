/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 //ZD 100886
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.IO;
using CustomizationUtil;


public partial class en_UITextChange : System.Web.UI.Page
{
    #region Data Members

    protected System.Web.UI.WebControls.DataGrid dgTxtChange;
    protected System.Web.UI.WebControls.Label errLabel;
    protected System.Web.UI.WebControls.Button btnSubmit;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnValue;
    private myVRMNet.NETFunctions obj;
  
    private CSSReplacementUtility cssUtil = null;

    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("UITextChange.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

            obj = new myVRMNet.NETFunctions();//FB 1830 - Translation
            if (Request.QueryString["m"] != null)
            if (Request.QueryString["m"].ToString().Equals("1"))
            {
                errLabel.Text = obj.GetTranslatedText("Operation Successful! Please logout and re-login to see the changes.");//FB 1830 - Translation
                errLabel.Visible = true;
            }

            //FB 2670
            if (Session["admin"].ToString() == "3")
            {
                
                //btnSubmit.ForeColor = System.Drawing.Color.Gray; // FB 2796
                //btnSubmit.Attributes.Add("Class", "btndisable");// FB 2796
                //ZD 100263
                btnSubmit.Visible = false;
            }
            else
                btnSubmit.Attributes.Add("Class", "altShort0BlueButtonFormat");// FB 2796

            if(!IsPostBack)
                BindData();
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    #endregion

    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        InitializeUIComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new System.EventHandler(this.Page_Load);

    }

    private void InitializeUIComponent()
    {
        dgTxtChange.ItemDataBound += new DataGridItemEventHandler(dgTxtChange_ItemDataBound);
       
    }

   
    #endregion

    #region Bind Data
     protected void BindData()
      {
          
        try
        {
            cssUtil = new CSSReplacementUtility();

            Hashtable ht1 = new Hashtable();
            Session["ApplicationPath"] = Server.MapPath(".");//FB 1830 - Translation Menu
            cssUtil.ApplicationPath = Session["ApplicationPath"].ToString();

            DataSet ds = cssUtil.GetTexts();

            dgTxtChange.DataSource = ds;
            dgTxtChange.DataBind();

            cssUtil.ReplaceTexts(ht1);

            btnSubmit.Attributes.Add("onclick", "javascript:return fnValidation(" + ds.Tables[0].Rows.Count + ")");
           
        }
        catch(Exception ex)
        {
            throw ex;             
         }
      }
    #endregion

    #region Bind Default Data
    protected void BindDefaultData()
    {

        try
        {
            hdnValue.Value = "1";
            cssUtil = new CSSReplacementUtility();

            Hashtable ht1 = new Hashtable();
            Session["ApplicationPath"] = Server.MapPath(".");//FB 1830 - Translation Menu
            cssUtil.ApplicationPath = Session["ApplicationPath"].ToString();

            DataSet ds = cssUtil.GetTexts();

            if (ds.Tables.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    dr["NewText"] = dr["myVRMDefault"].ToString();
                }
            }

            dgTxtChange.DataSource = ds;
            dgTxtChange.DataBind();

            cssUtil.ReplaceTexts(ht1);

            btnSubmit.Attributes.Add("onclick", "javascript:return fnValidation(" + ds.Tables[0].Rows.Count + ")");

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion
    
    #region Submit_ClickEvent Handler 
    protected void Submit_Click(object sender, EventArgs e)
    {
        try
        {
            cssUtil = new CSSReplacementUtility();

            Hashtable ht1 = new Hashtable();
            Session["ApplicationPath"] = Server.MapPath(".");//FB 1830 - Translation Menu
            cssUtil.ApplicationPath = Session["ApplicationPath"].ToString();
            foreach (DataGridItem dgi in dgTxtChange.Items)
            {
                if (dgi.ItemType.Equals(ListItemType.Item) || dgi.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    TextBox txtNewText = ((TextBox)dgi.FindControl("NewText"));
                    ht1.Add(dgi.Cells[0].Text.ToString(), txtNewText.Text);
                }
            }

            cssUtil.ReplaceTexts(ht1);

            cssUtil.ReplaceMenuText(hdnValue.Value);
            SetCompanyImages();

            Response.Redirect("UITextChange.aspx?m=1");
        }
        catch (Exception ex)
        {
            //throw ex;
        }
    }
    #endregion

    //Css Module starts...
    #region Set Organization Image
    /// <summary>
    /// Set Organization Image
    /// </summary>
    /// <returns></returns>
    private bool SetCompanyImages()
    {
       
        String errMsg = "";
        string inXML = "";
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();

            inXML = "<SetTextChangeXML>";
            inXML += obj.OrgXMLElement();//Organization Module 
            String cssXML = cssUtil.GetTextChangeXML();
            inXML += cssXML;

            inXML += "</SetTextChangeXML>";

            if (errMsg != "")
            {
                this.errLabel.Text = errMsg;
                this.errLabel.Visible = true;
                return false;
            }

            String outXML = obj.CallMyVRMServer("SetTextChangeXML", inXML, Application["MyVRMServer_ConfigPath"].ToString());
            if (outXML.IndexOf("<error>") >= 0)
            {
                this.errLabel.Text = obj.ShowErrorMessage(outXML);
                this.errLabel.Visible = true;
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            //this.errLabel.Text = e.Message;ZD 100263
            this.errLabel.Text = obj.ShowSystemMessage();
            this.errLabel.Visible = true;
            return false;
        }
    }
    #endregion
    //Css Module ends...

    #region Original Click Event Handler

    protected void Original_Click(object sender, EventArgs e)
    {
        try
        {
            BindDefaultData();
        }
        catch (Exception ex)
        {
            //throw ex;
        }
    }

    #endregion

    #region dgTxtChange_ItemDataBound

    private void dgTxtChange_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView row = e.Item.DataItem as DataRowView;
                if (row != null)
                {
                    TextBox newText = (TextBox)e.Item.FindControl("NewText");
                    String type = row["Type"].ToString();

                    //Added for FB 1425 QA Bug START
                    Label lblDesc = (Label)e.Item.FindControl("LblDesc");
                    Label curText = (Label)e.Item.FindControl("CurText");
                    
                    String display = row["Display"].ToString(); 

                    if (display == "0")
                    {
                        e.Item.Visible = false;
                    }

                    //Added for FB 1425 QA Bug END
                    if (type == "Menu")
                    {
                        if (row["Id"].ToString() == "2")
                            newText.MaxLength = 10;
                        else
                            newText.MaxLength = 14;
                    }
                    else if (type == "Page")
                        newText.MaxLength = 30;//Edited For FB 1428
                    else if (type == "Button")
                        newText.MaxLength = 25;//Edited for Virtual Bus requirements
                }             
            }
        }   

        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion
}
