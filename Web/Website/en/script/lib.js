/*ZD 100147 Start*/
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147  ZD 100886 End*/
// JScript source code

//this fn. is to find the mod of two numbers
function Mod(a, b) 
{
	return a - Math.floor(a/b)*b
}

//this fn. is to change the controls visibility


function ChangeControlVisibility(on)
{
	var inputControls;
	var selectControls;
	var buttonControls;
	var textareaControls;
	
	selectControls = document.getElementsByTagName('select');
	if (on)
	{
		DoVisible(selectControls);
	}
	else
	{
		DoHide(selectControls);
	}
}

//this fn. is to make the control array visible
function DoVisible(controlArray)
{
	for (var i=0; i<=controlArray.length-1; i++)
	{
		if (controlArray[i])
		{
						
			var this_ = controlArray[i].style
			if (this_)
				this_.visibility = 'visible';
		}
	}
}

//this fn. is to hide the control array
function DoHide(controlArray)
{
	for (var i=0; i<=controlArray.length-1; i++)
	{
		if (controlArray[i])
		{
			var this_ = controlArray[i].style

			if (this_)
				this_.visibility = 'hidden';
		}
	}
}

//this function is used to show and hide the div
function fnpopup()
{
	var args=fnpopup.arguments
	var n=args[0]
	var x=document.getElementById(n)
	var v=args[2];
	var d=args[1]
	if (x.style)
		{ 
		x=x.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; 
		}
	x.visibility=v;
}
			
// this fn. allows numeric value (on keypress)
function fnNumeric()
{
	if (event.keyCode>=48 && event.keyCode<=57 ||event.keyCode==46)
	{}
	else
	{
	event.returnValue=0;
	}
}

// this fn. allows only the whole number (on keypress)
function fnNumericonly()
{

	if (event.keyCode>=48 && event.keyCode<=57)
	{}
	else
	{
	event.returnValue=0;
	}
}

// this fn. allows only the proper decimal value (on keypress)
function fnDecimal()
{
	var args = fnDecimal.arguments;
	var obj = eval('document.FrmPM.'+ args[0]);
	
	if(obj)
	{
		var strtext = obj.value;
	
		if (event.keyCode>=48 && event.keyCode<=57 ||event.keyCode==46)
		{
			if(event.keyCode==46)
			{
				for(var i=0;i<=strtext.length;i++)
				{
					if(strtext.charAt(i)=='.')
					{
						event.returnValue=0;
					}
				} 
			}
		}
		else
		{
			event.returnValue=0;
		}
	}
}	

function numval(val,digits,minval,maxval)
{
	val = makeNumeric(val);
	if (val == "" || isNaN(val)) val = 0;
	val = parseFloat(val);
	if (digits != null)
	{
		var dec = Math.pow(10,digits);
		val = (Math.round(val * dec))/dec;
	}
	if (minval != null && val < minval) val = minval;
	if (maxval != null && val > maxval) val = maxval;
	return parseFloat(val);
}

//this fn. will return the numeric part alone
function makeNumeric(s)
{
	return filterChars(s, "1234567890.-");
}

//this fn. is used to filter the numeric part
function filterChars(s, charList)
{
	var s1 = "" + s; // force s1 to be a string data type
	var i;
	for (i = 0; i < s1.length; )
	{
		if (charList.indexOf(s1.charAt(i)) < 0)
			s1 = s1.substring(0,i) + s1.substring(i+1, s1.length);
		else
			i++;
	}
	return s1;
}

//Equivalent to VB Script FormatNumber fn.
function formatNumber(val,digits,minval,maxval)
{
	var sval = "" + numval(val,digits,minval,maxval);
	var i;
	var iDecpt = sval.indexOf(".");
	if (iDecpt < 0) iDecpt = sval.length;
	if (digits != null && digits > 0)
	{
		if (iDecpt == sval.length)
			sval = sval + ".";
		var places = sval.length - sval.indexOf(".") - 1;
		for (i = 0; i < digits - places; i++)
			sval = sval + "0";
	}
	var firstNumchar = 0;
	if (sval.charAt(0) == "-") firstNumchar = 1;
	for (i = iDecpt - 3; i > firstNumchar; i-= 3)
		sval = sval.substring(0, i) + "," + sval.substring(i);

	return sval;
}

//gives the differnce between two dates in terms of days
function presentDate(startdate,enddate) 
{
    var ierr = 1 ;
    
   // Verify whether the user wants to return only whole
   // intervals or intervals rounded to the nearest number 
   // of interval.
   //var roundDays = f.chkWholeDays.checked ;
   
   // Verify that the user entered something in the
   // Start Date input box.
   
    if(startdate != '') {
        if(!isNaN(Date.parse( startdate ))) {
            var s = new Date(Date.parse(startdate)) ;
            ierr = 0 ;
        }
    }
	// Verify that the user entered something in the
   // Ending Date input box.
  // alert(enddate)
    if(enddate != '' && ierr != 1)
    {
        if(!isNaN(Date.parse( enddate ))) {
            var e = new Date(Date.parse(enddate)) ;
            
            // call the dateDiff function.
            var temp = suycDateDiff( s, e , "d") ;
        }else{
            ierr = 1;
        }
    }
    else
    {
        ierr = 1;
    }
    
    // update the tellTime field with our new value.
//    if ( temp != null && ierr != 1 ) f.tellTime.value = temp.toString() ;


	return temp;
}

function suycDateDiff( start, end, interval, rounding ) {

    var iOut = 0;
    
    // Create 2 error messages, 1 for each argument. 
    var startMsg = "Check the Start Date and End Date\n"
        startMsg += "must be a valid date format.\n\n"
        startMsg += "Please try again." ;
		
    var intervalMsg = "Sorry the dateAdd function only accepts\n"
        intervalMsg += "d, h, m OR s intervals.\n\n"
        intervalMsg += "Please try again." ;

    var bufferA = Date.parse( start ) ;
    var bufferB = Date.parse( end ) ;
    	
    // check that the start parameter is a valid Date. 
    if ( isNaN (bufferA) || isNaN (bufferB) ) {
        alert( startMsg ) ;
        return null ;
    }
	
    // check that an interval parameter was not numeric. 
    if ( interval.charAt == 'undefined' ) {
        // the user specified an incorrect interval, handle the error. 
        alert( intervalMsg ) ;
        return null ;
    }
   
    var number = bufferB-bufferA ;
   // var number = bufferA-bufferB ;
    
    // what kind of add to do? 
    switch (interval.charAt(0))
    {
        case 'd': case 'D': 
            iOut = parseInt(number / 86400000) ;
            if(rounding) iOut += parseInt((number % 86400000)/43200001) ;
            break ;
        case 'h': case 'H':
            iOut = parseInt(number / 3600000 ) ;
            if(rounding) iOut += parseInt((number % 3600000)/1800001) ;
            break ;
        case 'm': case 'M':
            iOut = parseInt(number / 60000 ) ;
            if(rounding) iOut += parseInt((number % 60000)/30001) ;
            break ;
        case 's': case 'S':
            iOut = parseInt(number / 1000 ) ;
            if(rounding) iOut += parseInt((number % 1000)/501) ;
            break ;
        default:
        // If we get to here then the interval parameter
        // didn't meet the d,h,m,s criteria.  Handle
        // the error. 		
        alert(intervalMsg) ;
        return null ;
    }
    
    return iOut ;
}

//this fn. is used to compare two different dates
function fnCompareDate(Date1,Date2)
{
// This Function Returns the following values
// If Date2 > Date 1  - G (i.e date2 greater than date1)
// If Date2 = Date 1  - E (i.e date2 equal to date 1)
// If Date2 < Date 1  - L (i.e date2 lesser than date 1)
var ret

var mdy1 = Date1
var mdy2 = Date2
	
var mydate = new Date(mdy1)

var today= new Date(mdy2)

//month year and date of Date1
var entmonth1 = mydate.getMonth()
entmonth1=entmonth1 + 1
var entdate1 = mydate.getDate()
var entyear1 = mydate.getYear()

//month year and date of Date2
var entmonth2 =today.getMonth()
entmonth2=entmonth2+1
var entdate2 =today.getDate()
var entyear2 =today.getYear()
ret="L"
if (entyear2>entyear1)
	{ret="G"
	}
else
	{
		if(entmonth2 > entmonth1 && entyear2 == entyear1)
			{ret="G"
			}
		else
			{
			
			if(entdate2>entdate1 && entyear2 == entyear1 && entmonth2 == entmonth1)
				{ret="G"
				}
			else if(entdate2==entdate1 && entyear2 == entyear1 && entmonth2 == entmonth1)
				{
					ret="E"
				}
			}
	}
	
if (ret=="G")
{
//alert("date2 greater than date1")
return "G"
}
else if (ret=="E")
{
	//alert("date2 equal to date1")
	return "E"
}
else
	{
	//alert("date2 lesser date1")
	return "L"
	}
}

function cancelRefresh()
{
	// keycode for F5 function
	if (window.event && window.event.keyCode == 116) {
		window.event.keyCode = 8;
	}
	// keycode for backspace
	if (window.event && window.event.keyCode == 8) 
	{
		// try to cancel the backspace
		window.event.cancelBubble = true;
		window.event.returnValue = false;
		return false;
	}
}

if (document.all)
{	 	
	document.onkeydown = function ()
	{
		var key_f5 = 116; // 116 = F5		
	    if (key_f5==event.keyCode)
	    {
		//	alert("F5 pressed");
			window.event.keyCode = 8;
			return false;
		}
	}
}
	
//document.oncontextmenu = disableRightClick;
function disableRightClick()
{
  return false;
}

<!-- STEP ONE: Copy this code into a new file, save as date-picker.js  -->

<!-- Original:  Kedar R. Bhave (softricks@hotmail.com) -->
<!-- Web Site:  http://www.softricks.com -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

var weekend = [0,6];
var weekendColor = "#eeeeee";
var fontface = "Verdana";
var fontsize = 2;

var gNow = new Date();
var ggWinCal;
isNav = (navigator.appName.indexOf("Netscape") != -1) ? true : false;
isIE = (navigator.appName.indexOf("Microsoft") != -1) ? true : false;

Calendar.Months = ["January", "February", "March", "April", "May", "June",
"July", "August", "September", "October", "November", "December"];

// Non-Leap year Month days..
Calendar.DOMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
// Leap year Month days..
Calendar.lDOMonth = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

function Calendar(p_item, p_WinCal, p_month, p_year, p_format) {
	if ((p_month == null) && (p_year == null))	return;

	if (p_WinCal == null)
		this.gWinCal = ggWinCal;
	else
		this.gWinCal = p_WinCal;
	
	if (p_month == null) {
		this.gMonthName = null;
		this.gMonth = null;
		this.gYearly = true;
	} else {
		this.gMonthName = Calendar.get_month(p_month);
		this.gMonth = new Number(p_month);
		this.gYearly = false;
	}

	this.gYear = p_year;
	this.gFormat = p_format;
	this.gBGColor = "#eeeeee";
	this.gFGColor = "black";
	this.gTextColor = "black";
	this.gHeaderColor = "black";
	this.gReturnItem = p_item;
}

Calendar.get_month = Calendar_get_month;
Calendar.get_daysofmonth = Calendar_get_daysofmonth;
Calendar.calc_month_year = Calendar_calc_month_year;
Calendar.print = Calendar_print;

function Calendar_get_month(monthNo) {
	return Calendar.Months[monthNo];
}

function Calendar_get_daysofmonth(monthNo, p_year) {
	/* 
	Check for leap year ..
	1.Years evenly divisible by four are normally leap years, except for... 
	2.Years also evenly divisible by 100 are not leap years, except for... 
	3.Years also evenly divisible by 400 are leap years. 
	*/
	if ((p_year % 4) == 0) {
		if ((p_year % 100) == 0 && (p_year % 400) != 0)
			return Calendar.DOMonth[monthNo];
	
		return Calendar.lDOMonth[monthNo];
	} else
		return Calendar.DOMonth[monthNo];
}

function Calendar_calc_month_year(p_Month, p_Year, incr) {
	/* 
	Will return an 1-D array with 1st element being the calculated month 
	and second being the calculated year 
	after applying the month increment/decrement as specified by 'incr' parameter.
	'incr' will normally have 1/-1 to navigate thru the months.
	*/
	var ret_arr = new Array();
	
	if (incr == -1) {
		// B A C K W A R D
		if (p_Month == 0) {
			ret_arr[0] = 11;
			ret_arr[1] = parseInt(p_Year) - 1;
		}
		else {
			ret_arr[0] = parseInt(p_Month) - 1;
			ret_arr[1] = parseInt(p_Year);
		}
	} else if (incr == 1) {
		// F O R W A R D
		if (p_Month == 11) {
			ret_arr[0] = 0;
			ret_arr[1] = parseInt(p_Year) + 1;
		}
		else {
			ret_arr[0] = parseInt(p_Month) + 1;
			ret_arr[1] = parseInt(p_Year);
		}
	}
	
	return ret_arr;
}

function Calendar_print() {
	ggWinCal.print();
}

function Calendar_calc_month_year(p_Month, p_Year, incr) {
	/* 
	Will return an 1-D array with 1st element being the calculated month 
	and second being the calculated year 
	after applying the month increment/decrement as specified by 'incr' parameter.
	'incr' will normally have 1/-1 to navigate thru the months.
	*/
	var ret_arr = new Array();
	
	if (incr == -1) {
		// B A C K W A R D
		if (p_Month == 0) {
			ret_arr[0] = 11;
			ret_arr[1] = parseInt(p_Year) - 1;
		}
		else {
			ret_arr[0] = parseInt(p_Month) - 1;
			ret_arr[1] = parseInt(p_Year);
		}
	} else if (incr == 1) {
		// F O R W A R D
		if (p_Month == 11) {
			ret_arr[0] = 0;
			ret_arr[1] = parseInt(p_Year) + 1;
		}
		else {
			ret_arr[0] = parseInt(p_Month) + 1;
			ret_arr[1] = parseInt(p_Year);
		}
	}
	
	return ret_arr;
}

// This is for compatibility with Navigator 3, we have to create and discard one object before the prototype object exists.
new Calendar();

Calendar.prototype.getMonthlyCalendarCode = function() {
	var vCode = "";
	var vHeader_Code = "";
	var vData_Code = "";
	
	// Begin Table Drawing code here..
	vCode = vCode + "<TABLE cellspacing=1 cellpadding=3 BORDER=0 BGCOLOR=\"" + this.gBGColor + "\">";
	
	vHeader_Code = this.cal_header();
	vData_Code = this.cal_data();
	vCode = vCode + vHeader_Code + vData_Code;
	
	vCode = vCode + "</TABLE>";
	
	return vCode;
}

Calendar.prototype.show = function() {
	var vCode = "";
	
	this.gWinCal.document.open();

	// Setup the page...
	this.wwrite("<html>");
	this.wwrite("<head><title>Calendar</title>");
	this.wwrite("</head>");

	this.wwrite("<body bgcolor='#e8edf6' " + 
		"link=\"" + this.gLinkColor + "\" " + 
		"vlink=\"" + this.gLinkColor + "\" " +
		"alink=\"" + this.gLinkColor + "\" " +
		"text=\"" + this.gTextColor + "\">");
	this.wwriteA("<FONT FACE='" + fontface + "' SIZE=2><B>");
	this.wwriteA(this.gMonthName + " " + this.gYear);
	this.wwriteA("</B><BR>");

	// Show navigation buttons
	var prevMMYYYY = Calendar.calc_month_year(this.gMonth, this.gYear, -1);
	var prevMM = prevMMYYYY[0];
	var prevYYYY = prevMMYYYY[1];

	var nextMMYYYY = Calendar.calc_month_year(this.gMonth, this.gYear, 1);
	var nextMM = nextMMYYYY[0];
	var nextYYYY = nextMMYYYY[1];
	
	this.wwrite("<TABLE WIDTH='100%' BORDER=1 CELLSPACING=0 CELLPADDING=3 BGCOLOR='#eeeeee'><TR><TD ALIGN=center>");
	this.wwrite("[<A HREF=\"" +
		"javascript:window.opener.Build(" + 
		"'" + this.gReturnItem + "', '" + this.gMonth + "', '" + (parseInt(this.gYear)-1) + "', '" + this.gFormat + "'" +
		");" +
		"\"><<<\/A>]</TD><TD ALIGN=center>");
	this.wwrite("[<A HREF=\"" +
		"javascript:window.opener.Build(" + 
		"'" + this.gReturnItem + "', '" + prevMM + "', '" + prevYYYY + "', '" + this.gFormat + "'" +
		");" +
		"\"><<\/A>]</TD><TD ALIGN=center>");
	this.wwrite("[<A HREF=\"javascript:window.print();\">Print</A>]</TD><TD ALIGN=center>");
	this.wwrite("[<A HREF=\"" +
		"javascript:window.opener.Build(" + 
		"'" + this.gReturnItem + "', '" + nextMM + "', '" + nextYYYY + "', '" + this.gFormat + "'" +
		");" +
		"\">><\/A>]</TD><TD ALIGN=center>");
	this.wwrite("[<A HREF=\"" +
		"javascript:window.opener.Build(" + 
		"'" + this.gReturnItem + "', '" + this.gMonth + "', '" + (parseInt(this.gYear)+1) + "', '" + this.gFormat + "'" +
		");" +
		"\">>><\/A>]</TD></TR></TABLE><BR>");

	// Get the complete calendar code for the month..
	vCode = this.getMonthlyCalendarCode();
	this.wwrite(vCode);

	this.wwrite("</font></body></html>");
	this.gWinCal.document.close();
}

Calendar.prototype.showY = function() {
	var vCode = "";
	var i;
	var vr, vc, vx, vy;		// Row, Column, X-coord, Y-coord
	var vxf = 285;			// X-Factor
	var vyf = 200;			// Y-Factor
	var vxm = 10;			// X-margin
	var vym;				// Y-margin
	if (isIE)	vym = 75;
	else if (isNav)	vym = 25;
	
	this.gWinCal.document.open();

	this.wwrite("<html>");
	this.wwrite("<head><title>Calendar</title>");
	this.wwrite("<style type='text/css'>\n<!--");
	for (i=0; i<12; i++) {
		vc = i % 3;
		if (i>=0 && i<= 2)	vr = 0;
		if (i>=3 && i<= 5)	vr = 1;
		if (i>=6 && i<= 8)	vr = 2;
		if (i>=9 && i<= 11)	vr = 3;
		
		vx = parseInt(vxf * vc) + vxm;
		vy = parseInt(vyf * vr) + vym;

		this.wwrite(".lclass" + i + " bgcolor='#e8edf6' { position:absolute;top:" + vy + ";left:" + vx + ";}");
	}
	this.wwrite("-->\n</style>");
	this.wwrite("</head>");

	this.wwrite("<body bgcolor='#e8edf6' " + 
		"link=\"" + this.gLinkColor + "\" " + 
		"vlink=\"" + this.gLinkColor + "\" " +
		"alink=\"" + this.gLinkColor + "\" " +
		"text=\"" + this.gTextColor + "\">");
	this.wwrite("<FONT FACE='" + fontface + "' SIZE=2><B>");
	this.wwrite("Year : " + this.gYear);
	this.wwrite("</B><BR>");

	// Show navigation buttons
	var prevYYYY = parseInt(this.gYear) - 1;
	var nextYYYY = parseInt(this.gYear) + 1;
	
	this.wwrite("<TABLE WIDTH='100%' BORDER=1 CELLSPACING=0 CELLPADDING=0 BGCOLOR='#f8f8f8'><TR><TD ALIGN=center>");
	this.wwrite("[<A HREF=\"" +
		"javascript:window.opener.Build(" + 
		"'" + this.gReturnItem + "', null, '" + prevYYYY + "', '" + this.gFormat + "'" +
		");" +
		"\" alt='Prev Year'><<<\/A>]</TD><TD ALIGN=center>");
	this.wwrite("[<A HREF=\"javascript:window.print();\">Print</A>]</TD><TD ALIGN=center>");
	this.wwrite("[<A HREF=\"" +
		"javascript:window.opener.Build(" + 
		"'" + this.gReturnItem + "', null, '" + nextYYYY + "', '" + this.gFormat + "'" +
		");" +
		"\">>><\/A>]</TD></TR></TABLE><BR>");

	// Get the complete calendar code for each month..
	var j;
	for (i=11; i>=0; i--) {
		if (isIE)
			this.wwrite("<DIV ID=\"layer" + i + "\" CLASS=\"lclass" + i + "\">");
		else if (isNav)
			this.wwrite("<LAYER ID=\"layer" + i + "\" CLASS=\"lclass" + i + "\">");

		this.gMonth = i;
		this.gMonthName = Calendar.get_month(this.gMonth);
		vCode = this.getMonthlyCalendarCode();
		this.wwrite(this.gMonthName + "/" + this.gYear + "<BR>");
		this.wwrite(vCode);
		

		if (isIE)
			this.wwrite("</DIV>");
		else if (isNav)
			this.wwrite("</LAYER>");
	}

	this.wwrite("</font><BR></body></html>");
	this.gWinCal.document.close();
}

	Calendar.prototype.wwrite = function(wtext) {
	this.gWinCal.document.writeln(wtext);
}

Calendar.prototype.wwriteA = function(wtext) {
	this.gWinCal.document.write(wtext);
}

Calendar.prototype.cal_header = function() {
	var vCode = "";
	
	vCode = vCode + "<TR>";
	vCode = vCode + "<TD WIDTH='14%'><FONT SIZE='2' FACE='" + fontface + "' COLOR='" + this.gHeaderColor + "'><B>Sun</B></FONT></TD>";
	vCode = vCode + "<TD WIDTH='14%'><FONT SIZE='2' FACE='" + fontface + "' COLOR='" + this.gHeaderColor + "'><B>Mon</B></FONT></TD>";
	vCode = vCode + "<TD WIDTH='14%'><FONT SIZE='2' FACE='" + fontface + "' COLOR='" + this.gHeaderColor + "'><B>Tue</B></FONT></TD>";
	vCode = vCode + "<TD WIDTH='14%'><FONT SIZE='2' FACE='" + fontface + "' COLOR='" + this.gHeaderColor + "'><B>Wed</B></FONT></TD>";
	vCode = vCode + "<TD WIDTH='14%'><FONT SIZE='2' FACE='" + fontface + "' COLOR='" + this.gHeaderColor + "'><B>Thu</B></FONT></TD>";
	vCode = vCode + "<TD WIDTH='14%'><FONT SIZE='2' FACE='" + fontface + "' COLOR='" + this.gHeaderColor + "'><B>Fri</B></FONT></TD>";
	vCode = vCode + "<TD WIDTH='16%'><FONT SIZE='2' FACE='" + fontface + "' COLOR='" + this.gHeaderColor + "'><B>Sat</B></FONT></TD>";
	vCode = vCode + "</TR>";
	
	return vCode;
}

Calendar.prototype.cal_data = function() {
	var vDate = new Date();
	vDate.setDate(1);
	vDate.setMonth(this.gMonth);
	vDate.setFullYear(this.gYear);

	var vFirstDay=vDate.getDay();
	var vDay=1;
	var vLastDay=Calendar.get_daysofmonth(this.gMonth, this.gYear);
	var vOnLastDay=0;
	var vCode = "";

	/*
	Get day for the 1st of the requested month/year..
	Place as many blank cells before the 1st day of the month as necessary. 
	*/

	vCode = vCode + "<TR>";
	for (i=0; i<vFirstDay; i++) {
		vCode = vCode + "<TD WIDTH='14%'" + this.write_weekend_string(i) + "><FONT SIZE='2' FACE='" + fontface + "'> </FONT></TD>";
	}

	// Write rest of the 1st week
	for (j=vFirstDay; j<7; j++) {
		vCode = vCode + "<TD WIDTH='14%'" + this.write_weekend_string(j) + "><FONT SIZE='2' FACE='" + fontface + "'>" + 
			"<A HREF='#' " + 
				"onClick=\"self.opener.document." + this.gReturnItem + ".value='" + 
				this.format_data(vDay) + 
				"';self.opener.document." + this.gReturnItem + ".focus();window.close();\">" + 
				this.format_day(vDay) + 
			"</A>" + 
			"</FONT></TD>";
		vDay=vDay + 1;
	}
	vCode = vCode + "</TR>";

	// Write the rest of the weeks
	for (k=2; k<7; k++) {
		vCode = vCode + "<TR>";

		for (j=0; j<7; j++) {
			vCode = vCode + "<TD WIDTH='14%'" + this.write_weekend_string(j) + "><FONT SIZE='2' FACE='" + fontface + "'>" + 
				"<A HREF='#' " + 
					"onClick=\"self.opener.document." + this.gReturnItem + ".value='" + 
					this.format_data(vDay) + 
					"';self.opener.document." + this.gReturnItem + ".focus();window.close();\">" + 
				this.format_day(vDay) + 
				"</A>" + 
				"</FONT></TD>";
			vDay=vDay + 1;

			if (vDay > vLastDay) {
				vOnLastDay = 1;
				break;
			}
		}

		if (j == 6)
			vCode = vCode + "</TR>";
		if (vOnLastDay == 1)
			break;
	}
	
	// Fill up the rest of last week with proper blanks, so that we get proper square blocks
	for (m=1; m<(7-j); m++) {
		if (this.gYearly)
			vCode = vCode + "<TD WIDTH='14%'" + this.write_weekend_string(j+m) + 
			"><FONT SIZE='2' FACE='" + fontface + "' COLOR='gray'> </FONT></TD>";
		else
			vCode = vCode + "<TD WIDTH='14%'" + this.write_weekend_string(j+m) + 
			"><FONT SIZE='2' FACE='" + fontface + "' COLOR='gray'>" + m + "</FONT></TD>";
	}
	
	return vCode;
}

Calendar.prototype.format_day = function(vday) {
	var vNowDay = gNow.getDate();
	var vNowMonth = gNow.getMonth();
	var vNowYear = gNow.getFullYear();

	if (vday == vNowDay && this.gMonth == vNowMonth && this.gYear == vNowYear)
		return ("<FONT COLOR=\"RED\"><B>" + vday + "</B></FONT>");
	else
		return (vday);
}

Calendar.prototype.write_weekend_string = function(vday) {
	var i;

	// Return special formatting for the weekend day.
	for (i=0; i<weekend.length; i++) {
		if (vday == weekend[i])
			return (" BGCOLOR=\"" + weekendColor + "\"");
	}
	
	return "";
}

Calendar.prototype.format_data = function(p_day) {
	var vData;
	var vMonth = 1 + this.gMonth;
	vMonth = (vMonth.toString().length < 2) ? "0" + vMonth : vMonth;
	var vMon = Calendar.get_month(this.gMonth).substr(0,3).toUpperCase();
	var vFMon = Calendar.get_month(this.gMonth).toUpperCase();
	var vY4 = new String(this.gYear);
	var vY2 = new String(this.gYear.substr(2,2));
	var vDD = (p_day.toString().length < 2) ? "0" + p_day : p_day;

	switch (this.gFormat) {
		case "MM\/DD\/YYYY" :
			vData = vMonth + "\/" + vDD + "\/" + vY4;
			break;
		case "MM\/DD\/YY" :
			vData = vMonth + "\/" + vDD + "\/" + vY2;
			break;
		case "MM-DD-YYYY" :
			vData = vMonth + "-" + vDD + "-" + vY4;
			break;
		case "MM-DD-YY" :
			vData = vMonth + "-" + vDD + "-" + vY2;
			break;

		case "DD\/MON\/YYYY" :
			vData = vDD + "\/" + vMon + "\/" + vY4;
			break;
		case "DD\/MON\/YY" :
			vData = vDD + "\/" + vMon + "\/" + vY2;
			break;
		case "DD-MON-YYYY" :
			vData = vDD + "-" + vMon + "-" + vY4;
			break;
		case "DD-MON-YY" :
			vData = vDD + "-" + vMon + "-" + vY2;
			break;

		case "DD\/MONTH\/YYYY" :
			vData = vDD + "\/" + vFMon + "\/" + vY4;
			break;
		case "DD\/MONTH\/YY" :
			vData = vDD + "\/" + vFMon + "\/" + vY2;
			break;
		case "DD-MONTH-YYYY" :
			vData = vDD + "-" + vFMon + "-" + vY4;
			break;
		case "DD-MONTH-YY" :
			vData = vDD + "-" + vFMon + "-" + vY2;
			break;

		case "DD\/MM\/YYYY" :
			vData = vDD + "\/" + vMonth + "\/" + vY4;
			break;
		case "DD\/MM\/YY" :
			vData = vDD + "\/" + vMonth + "\/" + vY2;
			break;
		case "DD-MM-YYYY" :
			vData = vDD + "-" + vMonth + "-" + vY4;
			break;
		case "DD-MM-YY" :
			vData = vDD + "-" + vMonth + "-" + vY2;
			break;

		default :
			vData = vMonth + "\/" + vDD + "\/" + vY4;
	}

	return vData;
}

function Build(p_item, p_month, p_year, p_format) {
	var p_WinCal = ggWinCal;
	gCal = new Calendar(p_item, p_WinCal, p_month, p_year, p_format);

	// Customize your Calendar here..
	gCal.gBGColor="#ced4e0";
	gCal.gLinkColor="black";
	gCal.gTextColor="black";
	gCal.gHeaderColor="#4458b0";

	// Choose appropriate show function
	if (gCal.gYearly)	gCal.showY();
	else	gCal.show();
}

function show_calendar() {
	/* 
		p_month : 0-11 for Jan-Dec; 12 for All Months.
		p_year	: 4-digit year
		p_format: Date format (mm/dd/yyyy, dd/mm/yy, ...)
		p_item	: Return Item.
	*/
	p_item = arguments[0];
	if (arguments[1] == null)
		p_month = new String(gNow.getMonth());
	else
		p_month = arguments[1];
	if (arguments[2] == "" || arguments[2] == null)
		p_year = new String(gNow.getFullYear().toString());
	else
		p_year = arguments[2];
	if (arguments[3] == null)
		p_format = "MM/DD/YYYY";
	else
		p_format = arguments[3];

	vWinCal = window.open("", "Calendar", 
		"width=250,height=250,status=no,resizable=no,top=200,left=200");
	vWinCal.opener = self;
	ggWinCal = vWinCal;
	

	Build(p_item, p_month, p_year, p_format);

}
/*
Yearly Calendar Code Starts here
*/
function show_yearly_calendar(p_item, p_year, p_format) {
	// Load the defaults..
	if (p_year == null || p_year == "")
		p_year = new String(gNow.getFullYear().toString());
	if (p_format == null || p_format == "")
		p_format = "MM/DD/YYYY";

	var vWinCal = window.open("", "Calendar", "scrollbars=yes");
	vWinCal.opener = self;
	ggWinCal = vWinCal;

	Build(p_item, null, p_year, p_format);
}

/*  This fn. is equivalent to DateAdd() of VB Script. This will return date object's instance, 
	so retrieve the date,month,year seperately by using getDate(),etc., 
	and concatenate them to get the resultant date. 
*/	
function dateAdd( start, interval, number )
{   // Create 3 error messages, 1 for each argument.
	
	var startMsg = "Sorry the start parameter of the dateAdd function\n"
	    startMsg += "must be a valid date format.\n\n"
	    startMsg += "Please try again." ;
	
	var intervalMsg = "Sorry the dateAdd function only accepts\n"
	    intervalMsg += "d, h, m OR s intervals.\n\n"
	    intervalMsg += "Please try again." ;
	    
	var numberMsg = "Sorry the number parameter of the dateAdd function\n"
	    numberMsg += "must be numeric.\n\n"
	    numberMsg += "Please try again." ;
	// get the milliseconds for this Date object. 
	var buffer = Date.parse(start) ;
	
	// check that the start parameter is a valid Date. 
    if ( isNaN (buffer) ) 
	{ 
		alert( startMsg ) ;
	    return null ;
	}
	
	// check that an interval parameter was not numeric. 
	if ( interval.charAt == 'undefined' ) 
	{    
		// the user specified an incorrect interval, handle the error.
		alert( intervalMsg ) ;
		return null ;
	}
	
	// check that the number parameter is numeric. 
	if ( isNaN ( number ) )	
	{  alert( numberMsg ) ;
	    return null ;
	}
	
	// so far, so good...
	// what kind of add to do? 
	
	switch (interval.charAt(0))
	{
	    case 'd': case 'D':
						number *= 24 ; // days to hours
        case 'h': case 'H':
						number *= 60 ; // hours to minutes
        case 'm': case 'M':
						number *= 60 ; // minutes to seconds
        case 's': case 'S':
						number *= 1000 ; // seconds to milliseconds
						break ;
	    default:
						alert(intervalMsg) ;  // If we get to here then the interval parameter
						return null ;		  // didn't meet the d,h,m,s criteria.  Handle the error.
	} 

	return new Date( buffer + number ) ;
}

//Date validation

// Declaring valid date character, minimum year and maximum year
var dtCh= "/";
var minYear=1900;
var maxYear=2100;

//This function needs to be called for date validations
function IsValidDate(){
	
	var args=IsValidDate.arguments
	
	var dt=args[0]
	
	if(dtCh == '/')
	{
		if (isDate(dt)==false){
			return false
		}
		return true
	}
}

function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}

function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   } 
   return this
}

function isDate(dtStr){
	
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strMonth=dtStr.substring(0,pos1)
	var strDay=dtStr.substring(pos1+1,pos2)
	var strYear=dtStr.substring(pos2+1)
	
	strYr=strYear
	
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	if (pos1==-1 || pos2==-1){
		alert("The date format should be : mm/dd/yyyy")
		return false
	}
	if (strMonth.length<1 || month<1 || month>12){
		alert("Please enter a valid month")
		return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		alert("Please enter a valid day")
		return false
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		alert("Please enter a valid 4 digit year");
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		alert("Please enter a valid date")
		return false
	}
return true
}

//this fn. will return the security code by combining the coupon and maturity date
function fnFormSecuritycode(coupon, mdate)
{
	var splitcoupon,sec_coupon,sec_coupondec
	var mday,mmonth,myear,sec_day,sec_month,sec_year,sec_date,sec_code
	
	sec_code='';
	sec_coupon='';
	sec_date='';
	
	if(coupon >= 0)
	{
		if(coupon.indexOf(".")> 0)
		{
			splitcoupon = coupon.split(".");
			
			if(splitcoupon[0].length ==  1)
			{
				sec_coupon = '0' + splitcoupon[0];
			}
			else
			{
				sec_coupon = splitcoupon[0];
			}
			
			if(splitcoupon[1].length ==  1)
			{
				sec_coupondec = splitcoupon[1] + '0';
			}
			else
			{
				sec_coupondec = splitcoupon[1];
			}
		}
		else
		{
			if(coupon.length ==  1)
			{
				sec_coupon = '0' + coupon;
			}
			else
			{
				sec_coupon = coupon;
			}
			sec_coupondec = '00'
		
		}
		sec_coupon = sec_coupon + '.' + sec_coupondec + '%';
	}
	
	if(mdate != '')
	{
		var months = new Array("","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
		var pos1=mdate.indexOf(dtCh)
		var pos2=mdate.indexOf(dtCh,pos1+1)
		var mmonth=mdate.substring(0,pos1)
		var mday=mdate.substring(pos1+1,pos2)
		var myear=mdate.substring(pos2+3)
		
		if(mday.length ==  1)
		{
			sec_day = '0' + mday;
		}
		else
		{
			sec_day = mday;
		}
		
		if(mmonth.length > 1)
		{
			if(mmonth.charAt(0) == 0)
			{
				mmonth = mmonth.substring(1,mmonth.length)
			}
		}
		sec_month = months[mmonth];
		sec_year = myear;
		
		sec_date = sec_day + '-' + sec_month + '-' + sec_year;				
		
	}
	sec_code = sec_coupon + ' ' + sec_date;
	return sec_code;
}

//This function prints the contents of the div.
// this builds innerHtml concept and open a new popup window and writes the content into it
function printFriendly() 
{ 
	var gAutoPrint = true; // Flag for whether or not to automatically call the print function 
	if (document.getElementById!= null) 
	{ 
		var html = '<HTML>\n<HEAD>\n'; 

		if (document.getElementsByTagName!= null) 
		{ 
			var headTags = document.getElementsByTagName("head"); 
			if (headTags.length > 0) 
			html += headTags[0].innerHTML; 
		} 

		html += '\n</HE' + 'AD>\n<BODY>\n'; 
		
		//heading div
		var printhead1 = document.getElementById("head1"); 
		
		if (printhead1!= null) 
		{ 
			html += printhead1.innerHTML; 
		} 
		
		//sub heading div
		var printhead2 = document.getElementById("head2"); 
		if (printhead2!= null) 
		{ 
			html += printhead2.innerHTML; 
		} 
		
		//body
		var printPageElem = document.getElementById("taskHd"); 

		if (printPageElem!= null) 
		{ 
			html += printPageElem.innerHTML; 
		} 
		else 
		{ 
			alert("Could not find the printReady section in the HTML"); 
			return; 
		} 

		html += '\n</BO' + 'DY>\n</HT' + 'ML>'; 

		var printWin = window.open("","printFriendly"); 
		
		printWin.document.open(); 
		printWin.document.write(html); 
		printWin.document.close(); 
		
		if(gAutoPrint) 
			printWin.print(); 
	} 
	else 
	{ 
		alert("Sorry, the printer friendly feature works only in javascript enabled browsers."); 
	} 
} 

/* Bar charts
var months = new Array("January","February","March","April","May","June","July","August","September","October","November","December");
var days = new Array("Sunday","Monday","Tuesday","Wednsday","Thursday","Friday","Saturday");
var mtend = new Array(31,28,31,30,31,30,31,31,30,31,30,31);
var opt = new Array("Past","Future");
function getDateInfo() {
var y = document.form.year.value;
var m = document.form.month.options[document.form.month.options.selectedIndex].value;
var d = document.form.day.options[document.form.day.options.selectedIndex].value;
var hlpr = mtend[m];
if (d < mtend[m] + 1) {
if (m == 1 && y % 4 == 0) { hlpr++; }
var c = new Date(y,m,d);
var dayOfWeek = c.getDay();
document.form.dw.value = days[dayOfWeek];
if(c.getTime() > new Date().getTime()) {
document.form.time.value = opt[1];
}
else {
document.form.time.value = opt[0];
   }
}
else {
alert("The date "+months[m]+" "+d+", "+y+" is invalid.\nCheck it again.");
   }
}
function setY() {
var y = new Date().getYear();
if (y < 2000) y += 1900;
document.form.year.value = y;
}

*/
function printFriendly() 
	{ 
		var args = printFriendly.arguments;
		
		var gAutoPrint = true; // Flag for whether or not to automatically call the print function 
		var heading = args[1];
		
		if(document.getElementById != null) 
		{ 
			var html = '<HTML>\n<HEAD>\n'; 
			html += '<LINK href=Styles/Printstyles.css rel=stylesheet type=text/css >'
			html += '\n</HE' + 'AD>\n<BODY>\n <center>\n<b><font size=2> '+ heading +'</font></b><br><br><TABLE Border="1" Cellspacing="0" Cellpadding="4" bgcolor="white">\n'; 
				 
			var printPageElem = document.getElementById(args[0]); 

			if (printPageElem!= null) 
			{ 
				html += printPageElem.innerHTML; 
			} 
			else 
			{ 
				alert("Could not find the printReady section in the HTML"); 
				return; 
			} 
			
			html = html.replace('cellpadding="8"','cellpadding="0"')
			html = html.replace('cellpadding="8"','cellpadding="0"')
			/*html = html.replace('border=0','Border=1')
			html = html.replace('border=0','Border=1')
			html = html.replace('border=0','Border=1')
			html = html.replace('border=0','Border=1')
			*/
			html += '</TABLE>\n <center>\n</BO' + 'DY>\n</HT' + 'ML>'; 

			var printWin = window.open("","printFriendly"); 
			
			printWin.document.open(); 
			printWin.document.write(html); 
			printWin.document.close(); 

			if(gAutoPrint) 
				printWin.print(); 
			} 
			else 
			{ 
				alert("Sorry, the printer friendly feature works only in javascript enabled browsers."); 
			} 
		}
