﻿
<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_EditEntityOption.EditEntityOption" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script type="text/javascript">
    //ZD 100604 start
     var img = new Image();
     img.src = "../en/image/wait1.gif";
    //ZD 100604 End
    function frmValidator()
    {   
        if (!Page_ClientValidate())
               return Page_IsValid;
     
        var txtentityname = document.getElementById('<%=txtEntityName.ClientID%>');
        if(txtentityname != null)//FB 2491
        {
            if(txtentityname.value == "")
            {        
                reqEntityName.style.display = 'block';
                txtentityname.focus();
                return false;
            }
            else if (txtentityname.value.search(/^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/)==-1)
            {        
                regItemName1.style.display = 'block';
                txtentityname.focus();
                return false;
            }    
        }
        var txtentitydesc = document.getElementById('<%=txtEntityDesc.ClientID%>');
        if(txtentitydesc != null)//FB 2491
        {    
            if(txtentitydesc.value != "")
            {
                if (txtentitydesc.value.search(/^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/)==-1)
                {        
                    regItemDesc.style.display = 'block';
                    txtentitydesc.focus();
                    return false;
                }    
            }
        }
    return(true);
    }
    
    
    function frmClearValidator()
    {
        var regItemName1 = document.getElementById("regItemName1");
        if(regItemName1 != null)
            regItemName1.innerHTML = "";
        var regItemDesc = document.getElementById("regItemDesc");
        if(regItemDesc != null)
            regItemDesc.innerHTML = "";
            DataLoading(1);//ZD 100176
    
    }
    
    function FnConfirm(msg)
    {
              
        if(confirm(" " + msg + RSeditLinkedEntityCode))
            {
	            var btn = document.getElementById("btnHidden");
                btn.click();
	            return true;
            }
        else
            {
	            return false;
            }
    
    }
    //ZD 100176 start
    function DataLoading(val) {
        if (val == "1")
            document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
        else
            document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
    }
    //ZD 100176 End
    </script>

</head>
<body>
    <form id="frmEntityCode" autocomplete="off" runat="server"><%--ZD 101190--%>
    <div>
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
        <center>
            <table width="55%" border="0">
                <tr>
                    <td colspan="3">
                        <h3>
                            <asp:Label ID="lblHeader" runat="server"><asp:Literal ID="Literal1" Text='<%$ Resources:WebResources, OrganisationSettings_btnEntityCode%>' runat='server' /></asp:Label> <%--FB 2670--%>
                        </h3>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="3">
                        <h3>
                            <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                            <div id="dataLoadingDIV" name="dataLoadingDIV" align="center" style="display:none">
                                <img border='0' src='image/wait1.gif' alt="<asp:Literal Text='<%$ Resources:WebResources, Loading%>' runat='server' />" />
                            </div><%--ZD 100678--%>
                        </h3>
                    </td>
                </tr>
                <tr align="center">
                    <td align="center">
                        <table width="100%" border="0">
                            <tr>
                                <td colspan="4" height="30">
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td width="18%" align="left">  <%--ZD 100425--%>
                                <asp:Label ID="lblLang" runat="server" CssClass="blackblodtext" Text="<%$ Resources:WebResources, Language%>"></asp:Label> <%--ZD 100425--%>
                                </td>
                                <td align="left" style="width: 30%">
                                    <asp:Label id="lblOptionName" runat="server" cssclass="blackblodtext" text="<%$ Resources:WebResources, EditEntityOption_lblOptionName%>"></asp:Label>
                                </td>
                                <td align="left" style="width: 30%">
                                    <asp:Label id="lblOptionDesc" runat="server" cssclass="blackblodtext" text="<%$ Resources:WebResources, EditEntityOption_lblOptionDesc%>"></asp:Label>
                                </td>
                            </tr>
                            <tr id="trLang" runat="server">
                                <td valign="top" style="width: 3%"> <!-- FB 2050 -->
                                    <br />
                                    <asp:ImageButton ID="imgLangsOption" runat="server" ImageUrl="image/loc/nolines_minus.gif" AlternateText="<%$ Resources:WebResources, SettingSelect2_Expand%>"
                                        Height="20" Width="100%" vspace="0" hspace="0" ToolTip="<%$ Resources:WebResources, ViewOtherLanguages%>" /> <%--ZD 100419--%>
                                </td>
                                <td style="width: 18%" align="left" valign="top">
                                    <br />
                                    <a class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, EditEntityOption_English%>" runat="server"></asp:Literal></a>
                                </td>
                                <td align="left" valign="top" style="width: 40%">
                                    <br />
                                    <asp:TextBox ID="txtEntityID" Text="<%$ Resources:WebResources, EditEntityOption_txtEntityID%>" Visible="false" runat="server" Width="1%"></asp:TextBox>
                                    <asp:TextBox ID="txtEntityName" runat="server" Width="80%" CssClass="altText" MaxLength="35"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqEntityName" ValidationGroup="Submit" runat="server"
                                        ControlToValidate="txtEntityName" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regItemName1" ControlToValidate="txtEntityName"
                                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>"
                                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                </td>
                                <td align="left" valign="top" style="width: 34%">
                                    <br />
                                    <asp:TextBox ID="txtEntityDesc" Width="93%" runat="server" CssClass="altText" MaxLength="35"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="regItemDesc" ControlToValidate="txtEntityDesc"
                                        ValidationGroup="Submit" Display="dynamic" runat="server" SetFocusOnError="true"
                                        ErrorMessage="<%$ Resources:WebResources, InvalidCharacters5%>"
                                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                </td>
                                <td width="9%">
                                </td>
                            </tr>
                            <tr id="LangsOptionRow" runat="server" style="width: 100%" align="center">
                                <td style="width: 1%">
                                </td>
                                <td colspan="4" align="left">
                                    <asp:DataGrid ID="dgLangOptionlist" runat="server" AutoGenerateColumns="false" GridLines="None"
                                        CellPadding="0" Style="border-collapse: separate; border: 1;" ShowHeader="false"
                                        Width="100%">
                                        <ItemStyle Height="2" VerticalAlign="Top" />
                                        <Columns>
                                            <asp:BoundColumn DataField="OptionID" Visible="false"></asp:BoundColumn>
                                            <asp:TemplateColumn ItemStyle-CssClass="blackblodtext" ItemStyle-Width="19%" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLangName" runat="server" Text='<%# fnGetTranslatedText(DataBinder.Eval(Container, "DataItem.name").ToString()) %>'></asp:Label>
                                                    <asp:Label ID="lblLangID" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ID") %>'
                                                        Visible="false"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn  ItemStyle-HorizontalAlign="Left" ItemStyle-Width="41%">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtOptionName" runat="server" Width="80%" CssClass="altText" MaxLength="35"
                                                        Text='<%#DataBinder.Eval(Container, "DataItem.DisplayCaption") %>'></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="regItemName1" ControlToValidate="txtOptionName"
                                                        ValidationGroup="Submit" Display="dynamic" runat="server" SetFocusOnError="true"
                                                        ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>"
                                                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                                    <asp:TextBox ID="txtOptionID" Text='<%DataBinder.Eval(Container, "DataItem.OptionID") %>'
                                                        Visible="false" runat="server" Width="80%"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn >
                                                <ItemTemplate>
                                                    &nbsp;<asp:TextBox ID="txtOptionDesc" Width="80%" runat="server" CssClass="altText" MaxLength="35"
                                                        Text='<%#DataBinder.Eval(Container, "DataItem.HelpText") %>'></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="regItemDesc" ControlToValidate="txtOptionDesc"
                                                        ValidationGroup="Submit" Display="dynamic" runat="server" SetFocusOnError="true"
                                                        ErrorMessage="<%$ Resources:WebResources, InvalidCharacters6%>"
                                                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="4">
                                    <br />
									<%--ZD 100420--%>
                                     <button id="btnCancel" runat="server" class="altMedium0BlueButtonFormat" onserverclick="btnCancel_Click" onclick="frmClearValidator();"><asp:Literal text='<%$ Resources:WebResources, EditEntityOption_btnCancel%>' runat='server' /></button>
                                    <asp:Button ID="btnSubmit" runat="server" Text="<%$ Resources:WebResources, EditEntityOption_btnSubmit%>"  Width="100pt"
                                        OnClick="CreateNewEntityCode" OnClientClick="javascript:return frmValidator()" /> <%--FB 2796--%><%--ZD 100420--%>
                                    <asp:Button ID="btnUpdate" runat="server" Text="<%$ Resources:WebResources, EditEntityOption_btnUpdate%>" Width="100pt"
                                        OnClick="UpdateEntityCode" OnClientClick="javascript:return frmValidator()" /> <%--FB 2796--%>
									<%--ZD 100420--%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </center>
    </div>
    </form>
</body>
</html>

<script type="text/javascript" src="inc/softedge.js"></script>

<script type="text/javascript">

ExpandCollapse(document.getElementById("imgLangsOption"), "<%=LangsOptionRow.ClientID %>", true);

function ExpandCollapse(img, str, frmCheck)
{
    obj = document.getElementById(str);
    if (obj != null)
    {
        if (img != null)
        {
            if (frmCheck == true)
            {
                img.src = img.src.replace("minus", "plus");
                obj.style.display = "none";
            }
            if (frmCheck == false)
            {
                if (img.src.indexOf("minus") >= 0)
                {
                    img.src = img.src.replace("minus", "plus");
                    obj.style.display = "none";
                }
                else
                {
                    img.src = img.src.replace("plus", "minus");
                    obj.style.display = "";
                }
            }
        }
    }
    //ZD 100420
    if (document.getElementById('btnCancel') != null)
        document.getElementById('btnCancel').setAttribute("onblur", "document.getElementById('btnSubmit').focus(); document.getElementById('btnSubmit').setAttribute('onfocus', '');");
    if (document.getElementById('btnSubmit') != null)
        document.getElementById('btnSubmit').setAttribute("onblur", "document.getElementById('btnUpdate').focus(); document.getElementById('btnUpdate').setAttribute('onfocus', '');");  
} 
document.onkeydown = function(evt) {
    evt = evt || window.event;
    var keyCode = evt.keyCode;
    if (keyCode == 8) {
        if (document.getElementById("btnCancel") != null) { // backspace
            var str = document.activeElement.type;
            if (!(str == "text" || str == "textarea" || str == "password")) {
                document.getElementById("btnCancel").click();
                return false;
            }
        }
        if (document.getElementById("btnGoBack") != null) { // backspace
            var str = document.activeElement.type;
            if (!(str == "text" || str == "textarea" || str == "password")) {
                document.getElementById("btnGoBack").click();
                return false;
            }
        }
    }
    fnOnKeyDown(evt);
};
</script>
<%--FB 2500--%>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->