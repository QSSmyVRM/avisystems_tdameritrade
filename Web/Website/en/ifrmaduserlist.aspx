<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" AutoEventWireup="true"  Inherits="en_ifrmaduserlist" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>ifrmadduserlist</title>
    <script type="text/javascript"> // FB 2815
       var path = '<%=Session["OrgCSSPath"]%>';
       path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css";
       document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
    </script>   
    <script type="text/javascript" src="extract.js"></script>
    <script type="text/javascript" src="inc/disablerclick.js"></script>
    <script type="text/javascript" src="sorttable.js"></script> 
    <script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055--%>
</head>
<body>
<script type="text/javascript">
<!--  

//FB 1888
function replaceSpcChar(strName,symbol,srcSym)
{
   do
    {
      strName = strName.replace(srcSym, symbol);
    }while((strName.indexOf(srcSym)) >= "0");
    
    return strName;
}
function sortlist(id)
{
     
	SortRows(user_list, id);
	
}
function synselchkbox ()
{
   
	selallcb = eval("parent.document." + (f = queryField("f")) + ( (f == "frmMultipleusersetup") ? ".CheckAll" : ( (f == "frmAllocation") ? ".CheckAllAdUser" : "" ) ) );
	chkboxcb = document.frmIfrmuserlist.seladuser;
	
	allchked = true;
	if (chkboxcb != null) {
		if (chkboxcb.length == null) {
			allchked = chkboxcb.checked;
		} else {
			for (i = 0; i < chkboxcb.length; i++)
				allchked = (!chkboxcb[i].checked) ? false : allchked;
		}

		if (selallcb != null)
			selallcb.checked = (allchked) ? true : false;
	}
}

function adUserClicked(cb, uid, ufn, uln, ue) {
    window.parent.document.getElementById("CheckAllAdUser_Org").checked = false; // ZD 101362
    uln = replaceSpcChar(uln.replace(new RegExp("!!", "g"), "'"),'\"',"||"); //FB 1888    
    ufn = replaceSpcChar(ufn.replace(new RegExp("!!", "g"), "'"),'\"',"||"); //FB 1888
   	var reachmax = false;
	
	if (eval("parent.document." + queryField("f") + ".SelectedUser")) {
		curselusers = eval("parent.document." + queryField("f") + ".SelectedUser").value;
		seluserary = curselusers.split("||"); // FB 1888
		newselusers = ""
		found = -1;
		for (i = 0; i < seluserary.length-1; i++) {
			seluserary[i] = seluserary[i].split("``");
			if (seluserary[i][0] == uid) {
				found = i;
			}
		}
		

		if (cb.checked) {
			if (found == -1) {
				if ( seluserary.length <= 50) {
					newselusers += curselusers + uid + "``" + ufn + "``" + uln + "``" + ue + "``1||"; // FB 1888
				} else {
					alert("Error: add new myVRM user fail - only the first 50 users are added.\nReason: maximum total number of myVRM users is 50 at a time.");
					cb.checked = false;
					newselusers = curselusers;
					
					reachmax = true;
				}
			} else {
				for (i = 0; i <found; i++)
					newselusers += seluserary[i][0] + "``" + seluserary[i][1] + "``" + seluserary[i][2] + "``" + seluserary[i][3] + "``" + seluserary[i][4] + "||"; // FB 1888
				newselusers += seluserary[found][0] + "``" + seluserary[found][1] + "``" + seluserary[found][2] + "``" + seluserary[found][3] + "``" + (parseInt(seluserary[found][4], 10) + 1) + "||"; // FB 1888
				for (i = found + 1; i < seluserary.length-1; i++)
					newselusers += seluserary[i][0] + "``" + seluserary[i][1] + "``" + seluserary[i][2] + "``" + seluserary[i][3] + "``" + seluserary[i][4] + "||";// FB 1888
			}
			
		} else {
			if (found != -1) {
				for (i = 0; i <found; i++)
					newselusers += seluserary[i][0] + "``" + seluserary[i][1] + "``" + seluserary[i][2] + "``" + seluserary[i][3] + "``" + seluserary[i][4] + "||"; // FB 1888
				if (queryField("f") == "frmAllocation") {
					if (parseInt(seluserary[found][4], 10) > 1) {
						newselusers += seluserary[found][0] + "``" + seluserary[found][1] + "``" + seluserary[found][2] + "``" + seluserary[found][3] + "``" + (parseInt(seluserary[found][4], 10) - 1) + "||"; // FB 1888
					}
				}
				for (i = found + 1; i < seluserary.length-1; i++)
					newselusers += seluserary[i][0] + "``" + seluserary[i][1] + "``" + seluserary[i][2] + "``" + seluserary[i][3] + "``" + seluserary[i][4] + "||"; // FB 1888
			}

		}
		
		eval("parent.document." + queryField("f") + ".SelectedUser").value = newselusers;
		parent.ifrmVRMuserlist.location.reload();
		
		synselchkbox();
	}
	
	return (reachmax);
}

// ZD 101362 Start
function fnHideIframe(par) {
    if (par == "1")
        document.getElementById("hideScreen").style.display = "block";
    else
        document.getElementById("hideScreen").style.display = "none";
}
// ZD 101362 Ends

//-->
</script>





<form id="frmIfrmuserlist" method="POST" action="dispatcher/admindispatcher.asp" runat="server">
<div id="hideScreen" style="width:500px; height:500px; z-index: 1000; position:fixed; left:0px; top:0px; display:none; background-color:lightgray">
<span class="blackblodtext"><br />&nbsp;&nbsp;<asp:Literal ID="Literal1" Text='<%$ Resources:WebResources, SelectedAllUsers%>' runat='server'></asp:Literal></span>
</div>  <%--ZD 101362--%>
 <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
        <%-- ZD 101362
        <table width="95%">
            <tr align="center">
                <td>
                    <asp:Label ID="LblError" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
        </table>--%>
 <input type="hidden" name="adusers" id="adusers" runat="server" />
<input name="hdnusers" id="hdnusers" type="hidden" runat="server" />
<input name="Action" id="Action" type="hidden" />
<input name="SelectedUser" id="SelectedUser" type="hidden" runat="server" />
<input name="sortby" id="sortby" type="hidden"  runat="server" />
<input name="alphabet" id="alphabet" type="hidden"  runat="server" />
<input name="pageno" id="pageno" type="hidden"  runat="server" />
<input name="timezones" id="timezones" type="hidden"  runat="server" />
<input name="roles" id="roles" type="hidden"  runat="server" />
<input name="languages" id="languages" type="hidden"  runat="server" />
<input name="bridges" id="bridges" type="hidden"  runat="server" />
<input name="departments" id="departments" type="hidden"  runat="server" />
<input name="canAll" id="canAll" type="hidden"  runat="server" />
<input name="totalPages" id="totalPages" type="hidden"  runat="server" />
<input name="totalNumber" id="totalNumber" type="hidden"  runat="server" />
<input id="helpPage" type="hidden" value="94" />
  <center>

<script type="text/javascript">
<!--
	   
	    if ( (!eval("parent.document." + queryField("f"))) || (!parent.ifrmVRMuserlist) || (!parent.chgpageoption) ) {
		    setTimeout('window.location.reload();',500);
	    }
    	
	    xmlstr = document.frmIfrmuserlist.hdnusers.value;
	    var varnousr = RSNousers;
        
        if (parseInt(document.frmIfrmuserlist.totalPages.value, 10) == 0) {
            document.write("<br><br><font color=red>" + RSNousers + ".</font>")
        }
	    
	    parent.chgpageoption (document.frmIfrmuserlist.totalPages.value);
    	
	    if (parseInt(document.frmIfrmuserlist.totalPages.value, 10) > 0)
		    parent.pageimg (document.frmIfrmuserlist.pageno.value)

	    parent.typeimg ("ifrmADuserlist", document.frmIfrmuserlist.sortby.value)
    	
	    parent.nameimg ("u", document.frmIfrmuserlist.alphabet.value)

	    var user_list = new SortTable('user_list');
    	
	    user_list.AddColumn("Select","width=50","center","form");
	    user_list.AddColumn("Lstname","width=160","center","email");
	    user_list.AddColumn("Frtname","width=160","center","email");
	    user_list.AddColumn("Email","width=190","center","email");


	    usersary = xmlstr.split("||"); // FB 1888
    	
	    for (i = 0; i < usersary.length-1; i++) {
		    usersary[i] = usersary[i].split("``");
    		
		    if(usersary[i][3].length > 12){
			    strLongEmail = usersary[i][3].substr(0,10) + "..."
		    }
		    else{
			    strLongEmail = usersary[i][3]
		    }
    		
		    if( (usersary[i][0] != "11") && (usersary[i][0] != '<%=Session["userID"] %>')) //FB 2026
		    {
		        selectstr = "<input type='checkbox' name='seladuser' value='" + usersary[i][0] + "' onClick='adUserClicked(this, \"" + usersary[i][0] + "\", \"" + replaceSpcChar(usersary[i][1].replace(new RegExp("'", "g"), "!!"),'||',"\"") + "\", \"" + replaceSpcChar(usersary[i][2].replace(new RegExp("'", "g"), "!!"),'||',"\"") + "\", \"" + usersary[i][3] + "\");'>"; // FB 1888
		        emailstr = "<a href='mailto:" + usersary[i][3] + "' title='" + usersary[i][3] + "'>" + strLongEmail + "</a>"              // FB 1888
		        //selectstr = "<input type='checkbox' name='seladuser' value='" + usersary[i][0] + "' onClick='adUserClicked(this, ||" + usersary[i][0] + "||,||" + usersary[i][1] + "||, ||" + usersary[i][2] + "||, ||" + usersary[i][3] + "||);'>";
		        //emailstr = "<a href='mailto:" + usersary[i][3] + "' title='" + usersary[i][3] + "'>" + strLongEmail + "</a>"
		        //selectstr = "<input type='checkbox' name='seladuser' value='" + usersary[i][0] + "' onClick='adUserClicked(this, \"" + usersary[i][0] + "\", \"" + usersary[i][1] + "\", \"" + usersary[i][2] + "\", \"" + usersary[i][3] + "\");'>";
		        //emailstr = "<a href='mailto:" + usersary[i][3] + "'>" + usersary[i][3] + "</a>"
		        user_list.AddLine(selectstr, usersary[i][1], usersary[i][2], emailstr);
		    }
	    }
	    
//--> 
</script>
	<table width='100%' border='0' cellpadding='2' cellspacing='1'>
      <script type="text/javascript">user_list.WriteRows()</script>
    </table>

  </center>

  
</form>


<script type="text/javascript">
<!--

	if (queryField("f") == "frmMultipleusersetup") {
		parent.syn();
		synselchkbox();
	}
		
//-->
</script>

</body>

</html>
